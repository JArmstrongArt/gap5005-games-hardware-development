﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

enum instList_levelTreatment
{
    Left,
    Right,
    Middle
}
public class LevelDisplay : MonoBehaviour
{
    [SerializeField] GameObject levelObject_prefab;

    private GameObject[] levelObject_instList;
    private int allLevels_currentLevelIndex;
    [SerializeField] GameObject parentCanvas_inst;
    [SerializeField] Sprite star_gold;
    private bool dbg_leftRight_axisUsed;
    [SerializeField] GameObject bestTimesBtnObj;
    [SerializeField] AudioSource pingSrc;
    // Start is called before the first frame update
    public int getCurrentLevelIndex()
    {
        return allLevels_currentLevelIndex;
    }
    
    void Awake()
    {
        levelObject_instList = new GameObject[3];
        RefreshLevelView(allLevels_currentLevelIndex);
    }

    void BestTimesBtnVisible(bool state)
    {
        if (bestTimesBtnObj.GetComponent<Button>() != null)
        {
            bestTimesBtnObj.GetComponent<Button>().enabled = state;
        }

        if (bestTimesBtnObj.GetComponent<Image>() != null)
        {
            bestTimesBtnObj.GetComponent<Image>().enabled = state;
        }

        if (bestTimesBtnObj.transform.childCount >0)
        {
            for(int i = 0; i < bestTimesBtnObj.transform.childCount; i++)
            {
                if (bestTimesBtnObj.transform.GetChild(i).transform.GetComponent<Image>() != null)
                {
                    bestTimesBtnObj.transform.GetChild(i).transform.GetComponent<Image>().enabled = state;
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Constants.DEBUGMODE == true)
        {
            if (Input.GetAxis("Debug_LeftRight") != 0)
            {
                if (dbg_leftRight_axisUsed == false)
                {
                    allLevels_currentLevelIndex += Mathf.RoundToInt(Input.GetAxis("Debug_LeftRight"));
                    RefreshLevelView(allLevels_currentLevelIndex);
                    dbg_leftRight_axisUsed = true;
                    
                }
            }
            else
            {
                dbg_leftRight_axisUsed = false;
            }
        }
        
    }

    void RefreshLevelView(int newSelected)
    {
        if (AllLevels.levelList_initialLoad_complete == true)
        {
            if (newSelected < 0)
            {
                newSelected = 0;
            }

            if (newSelected > AllLevels.levelList.Length - 1)
            {
                newSelected = AllLevels.levelList.Length - 1;
            }

            allLevels_currentLevelIndex = newSelected;

            for (int i = 0; i < levelObject_instList.Length; i++)
            {
                RefreshLevelView_IndividualLevel(i);
            }

            BestTimesBtnVisible(AllLevels.levelList[allLevels_currentLevelIndex].completed);
        }

    }

    void RefreshLevelView_IndividualLevel(int levelInst_index)
    {
        instList_levelTreatment instList_levelTreatment_enum;
        int instList_closestMiddleIndex = Mathf.FloorToInt(levelObject_instList.Length * 0.5f);
        if (levelInst_index < instList_closestMiddleIndex)
        {
            instList_levelTreatment_enum = instList_levelTreatment.Left;
        }else if (levelInst_index > instList_closestMiddleIndex)
        {
            instList_levelTreatment_enum = instList_levelTreatment.Right;
        }
        else
        {
            instList_levelTreatment_enum = instList_levelTreatment.Middle;
        }

        GameObject levelObject_inst;
        if (levelObject_instList[levelInst_index] != null)
        {
            levelObject_inst = levelObject_instList[levelInst_index];
        }
        else
        {
            levelObject_inst = Instantiate(levelObject_prefab, parentCanvas_inst.transform);
            levelObject_instList[levelInst_index] = levelObject_inst;
        }

        RectTransform levelObject_inst_rect = levelObject_inst.GetComponent<RectTransform>();

        float anchoredPosition_sideLimit = 0.293f;
        float anchoredPosition_heightLimit = 0.16f;

        float anchoredPosition_sideLimit_finalCalc = (anchoredPosition_sideLimit / instList_closestMiddleIndex) * Mathf.Abs (instList_closestMiddleIndex-levelInst_index);
        switch (instList_levelTreatment_enum)
        {
            case (instList_levelTreatment.Left):
                levelObject_inst_rect.anchoredPosition = new Vector2(-anchoredPosition_sideLimit_finalCalc, -anchoredPosition_heightLimit);
                
                break;
            case (instList_levelTreatment.Right):
                levelObject_inst_rect.anchoredPosition = new Vector2(anchoredPosition_sideLimit_finalCalc, -anchoredPosition_heightLimit);

                break;
            case (instList_levelTreatment.Middle):
                levelObject_inst_rect.anchoredPosition = Vector2.zero;

                break;
        }

        switch (instList_levelTreatment_enum)
        {
            case (instList_levelTreatment.Middle):
                levelObject_inst_rect.localScale = Vector3.one;

                break;

            default:
                levelObject_inst_rect.localScale = new Vector3(0.5f, 0.5f, 1);
                break;
        }

        LevelObject_ImportantValues importantScr = levelObject_instList[levelInst_index].GetComponent<LevelObject_ImportantValues>();
        int importantScr_sourceInfoIndex = -1;
        switch (instList_levelTreatment_enum)
        {
            case (instList_levelTreatment.Middle):
                importantScr_sourceInfoIndex = allLevels_currentLevelIndex;

                break;

            case (instList_levelTreatment.Left):
                importantScr_sourceInfoIndex = allLevels_currentLevelIndex - Mathf.Abs(instList_closestMiddleIndex - levelInst_index);

                break;

            case (instList_levelTreatment.Right):
                importantScr_sourceInfoIndex = allLevels_currentLevelIndex + Mathf.Abs(instList_closestMiddleIndex - levelInst_index);
                break;
        }

        switch (instList_levelTreatment_enum)
        {
            case (instList_levelTreatment.Middle):
                importantScr.loadLevelBtn.enabled = true;
                break;
            default:
                importantScr.loadLevelBtn.enabled = false;
                break;
        }

        if(importantScr_sourceInfoIndex>=0 && importantScr_sourceInfoIndex< AllLevels.levelList.Length)
        {
            importantScr.levelToLoadName = AllLevels.levelList[importantScr_sourceInfoIndex].sceneName;
            importantScr.SimpleVisibility(true);
            Level sourceLev = AllLevels.levelList[importantScr_sourceInfoIndex];

            importantScr.iconImg.sprite = sourceLev.icon;

            if (sourceLev.unlocked == false)
            {
                importantScr.lockIcon.enabled = true;
                
                importantScr.iconImg.enabled = false;
                importantScr.timerTxt.enabled = false;
                importantScr.timerIcon.enabled = false;
                importantScr.timerTxt.text = "";
                importantScr.starSetObj.SetRank(0);
                importantScr.starSetObj.Visible(false);
            }
            else
            {
                importantScr.lockIcon.enabled = false;

                importantScr.iconImg.enabled = true;
                importantScr.timerTxt.enabled = sourceLev.completed;
                importantScr.timerIcon.enabled = sourceLev.completed;
                importantScr.starSetObj.Visible(sourceLev.completed);
                if (sourceLev.completed == true)
                {

                    if (sourceLev.bestTimes.Length <= 0 || (sourceLev.bestTimes.Length > 0 && sourceLev.bestTimes[0] == Constants.INVALID_TIME))
                    {
                        importantScr.timerTxt.text = "NO TIME DATA";
                    }
                    else
                    {
                        importantScr.timerTxt.text = EngineGameTime.GameTime_Formalize(AllLevels.levelList[importantScr_sourceInfoIndex].bestTimes[0]);
                    }
                    if (instList_levelTreatment_enum == instList_levelTreatment.Left)
                    {
                        print("sourceinfoindex: "+importantScr_sourceInfoIndex.ToString());
                    }
                    importantScr.starSetObj.SetRank(AllLevels.levelList[importantScr_sourceInfoIndex].starRanking);

                }
                else
                {
                    importantScr.timerTxt.text = "";
                    importantScr.starSetObj.SetRank(0);

                }
            }
        }
        else
        {
            importantScr.levelToLoadName = "";
            importantScr.SimpleVisibility(false);
        }

        



        //instList_closestMiddleIndex + (instList_closestMiddleIndex-levelInst_index)
    }

    public void ChangeLevel(int changeAmount)
    {
        pingSrc.Play();
        allLevels_currentLevelIndex += changeAmount;
        RefreshLevelView(allLevels_currentLevelIndex);
    }
}
