﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodeGeneric : MonoBehaviour
{
    private bool postExplodeMode;
    [SerializeField] float explodePower;
    [SerializeField] Vector2 explodeDirection_xRange;
    [SerializeField] float explodeDirection_xRange_voidRange;
    [SerializeField] Vector2 explodeDirection_yRange;
    [SerializeField] Vector2 explodeDirection_zRange;
    [SerializeField] Vector3 modelRotatePower;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    float xRangeCalculator()
    {
        float ret = Random.Range(explodeDirection_xRange.x, explodeDirection_xRange.y);

        while (ret >= -Mathf.Abs(explodeDirection_xRange_voidRange) && ret <= Mathf.Abs(explodeDirection_xRange_voidRange))
        {
            ret = Random.Range(explodeDirection_xRange.x, explodeDirection_xRange.y);
        }
        return ret;
    }


    public void Explode(Rigidbody rb,float weightOfObject)//run this frame after frame in a fixedupdate loop for it to work properly
    {
        if (postExplodeMode == false)
        {
            //hard-coding the rigidbody parameters used so that designers can't make this script act as unintended.
            rb.mass = weightOfObject;
            rb.drag = 0.0f;
            rb.angularDrag = 0.0f;
            rb.useGravity = false;//unity gravity does not let your object fall faster even at higher masses, i dont understand why, so i reinterpret unity gravity in an addforce call
            rb.isKinematic = false;
            rb.interpolation = RigidbodyInterpolation.None;
            rb.collisionDetectionMode = CollisionDetectionMode.Discrete;

            Vector3 eulerDir = new Vector3(xRangeCalculator(), Random.Range(explodeDirection_yRange.x, explodeDirection_yRange.y), Random.Range(explodeDirection_zRange.x, explodeDirection_zRange.y));
            GameObject dirObj = new GameObject();
            dirObj.name = "player_explode_dirObj";
            dirObj.transform.position = rb.gameObject.transform.position;
            dirObj.transform.rotation = Quaternion.Euler(eulerDir);
            rb.AddForce(dirObj.transform.up * explodePower, ForceMode.Impulse);
            Destroy(dirObj);
            postExplodeMode = true;
        }

        if (postExplodeMode == true)
        {
            rb.gameObject.transform.Rotate(modelRotatePower.x * Time.fixedDeltaTime, modelRotatePower.y * Time.fixedDeltaTime, modelRotatePower.z * Time.fixedDeltaTime);
        }

        TweakedPhysics.RunTweakedGravity(rb);
    }
}
