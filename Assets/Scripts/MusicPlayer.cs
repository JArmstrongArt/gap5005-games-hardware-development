﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class MusicTrack
{
    public AudioClip musicSource;
    public string musicName;
}
public class MusicPlayer : MonoBehaviour
{
    [SerializeField] MusicTrack[] allSongs;
    private AudioSource songSrc;
    private void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
        songSrc = GetComponent<AudioSource>();
    }

    public void PlayMusic(AudioClip song)
    {
        if (songSrc.isPlaying == false||songSrc.clip!=song)
        {
            songSrc.clip = song;
            songSrc.Play();
        }

    }

    public void StopMusic()
    {
        songSrc.Stop();
    }

    public AudioClip GetSongByName(string musicName)
    {
        AudioClip ret = null;
        if (allSongs.Length > 0)
        {
            for (int i = 0; i < allSongs.Length; i++)
            {
                if (allSongs[i].musicSource != null)
                {
                    if (allSongs[i].musicName.ToLower() == musicName.ToLower())
                    {
                        ret = allSongs[i].musicSource;
                        break;
                    }
                }
            }
        }
        return ret;

    }


}