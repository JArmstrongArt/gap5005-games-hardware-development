﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RankingCalculator : MonoBehaviour
{

    public static int rankingCalculation(PlayerDamage dmgScr, CountupTimer timerScr_runningSinceSceneBegan, Level levelScr)
    {
        float dmg = dmgScr.dmg_taken;
        float dmg_max = dmgScr.dmg_taken_max;
        float time_taken = timerScr_runningSinceSceneBegan.engineTime_get();
        float time_average = levelScr.avgTimeToBeat_engineTime;

        int dmg_ranking = Mathf.RoundToInt(((1 / dmg_max) * (dmg_max-dmg)) * 5);

        int time_ranking = -1;
        if (time_taken <= time_average)
        {
            time_ranking = 5;
        }else if(time_taken > time_average && time_taken <= time_average * 1.25f)
        {
            time_ranking = 4;
        }
        else if (time_taken > time_average * 1.25f && time_taken <= time_average * 1.5f)
        {
            time_ranking = 3;
        }
        else if (time_taken > time_average * 1.5f && time_taken <= time_average * 1.75f)
        {
            time_ranking = 2;
        }
        else
        {
            time_ranking = 1;
        }

        int ranking_final = (dmg_ranking + time_ranking) / 2;
        if (ranking_final > 5)
        {
            ranking_final = 5;
        }

        if (ranking_final < 1)
        {
            ranking_final = 1;
        }
        return ranking_final;

    }
}
