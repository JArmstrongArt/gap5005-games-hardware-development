﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CountupTimer : MonoBehaviour
{
    public bool paused;
    private float engineTime;
    private string gameTime;
    // Start is called before the first frame update

    private void Awake()
    {
        ResetTimer();
    }
    // Update is called once per frame
    void Update()
    {
        if (paused == false)
        {
            engineTime += Time.deltaTime;
            gameTime = EngineGameTime.EngineTimeToGameTime(engineTime);
        }
    }

    public string gameTime_get()
    {
        return gameTime;
    }

    public float engineTime_get()
    {
        return engineTime;
    }

    public void ResetTimer()
    {
        engineTime = 0.0f;
        gameTime = EngineGameTime.EngineTimeToGameTime(engineTime);//fsr when i dont do this for a string of all things, i get a nullreferenceexception, which ive never seen before on a string
    }
}
