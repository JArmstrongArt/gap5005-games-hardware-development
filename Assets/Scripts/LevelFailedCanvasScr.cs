﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class LevelFailedCanvasScr : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        RemoveHud();
        ChangeMusic("dead");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GoToLevelSelect()
    {
        MenuBlockInitSpawner.spawnCube = UpcomingMenuCube.LevelSelect;
        ChangeMusic("gameplay");
        SceneManager.LoadScene("mainMenu");
    }

    public void RetryLevel()
    {
        ChangeMusic("gameplay");
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);

    }

    void RemoveHud()
    {
        if (GameObject.FindGameObjectWithTag("hudCanvas") != null)
        {
            Destroy(GameObject.FindGameObjectWithTag("hudCanvas"));
        }
    }
    
    void ChangeMusic(string musName)
    {
        if (GameObject.FindGameObjectWithTag("musicPlayer") != null)
        {

            if (GameObject.FindGameObjectWithTag("musicPlayer").GetComponent<MusicPlayer>() != null)
            {
                MusicPlayer musicPlayer_scr = GameObject.FindGameObjectWithTag("musicPlayer").GetComponent<MusicPlayer>();
                musicPlayer_scr.PlayMusic(musicPlayer_scr.GetSongByName(musName));
            }
        }
    }
}
