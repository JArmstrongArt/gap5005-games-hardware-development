﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spin : MonoBehaviour
{
    public Vector3 spinFactor;

    [HideInInspector]
    public Vector3 spinFactor_current;
    // Start is called before the first frame update
    void Awake()
    {

        spinFactor_current = spinFactor;
    }

    // Update is called once per frame
    void Update()
    {
        spinFactor_current = Vector3.Lerp(spinFactor_current, spinFactor, 5 * Time.deltaTime);
        transform.Rotate(new Vector3(spinFactor_current.x * Time.deltaTime, spinFactor_current.y * Time.deltaTime, spinFactor_current.z * Time.deltaTime));
    }
}
