﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
public enum UpcomingMenuCube
{
    TitleScreen,
    LevelSelect,
    BestTimes,
    Options,
    Credits,
    HowToPlay
}
public class MenuCubeControl : MonoBehaviour
{
    private Vector3 respawnPos = new Vector3(0, 10, 0);
    private bool explodeMode;
    [SerializeField] float timeToHoldOnExplosion;
    private float timeToHoldOnExplosion_orig;
    [SerializeField] float timeToDeleteExplosion;
    private float timeToDeleteExplosion_orig;
    private ExplodeGeneric explodeScr;
    private Rigidbody myRb;
    private UpcomingMenuCube menuCube_enum;
    [SerializeField] GameObject titleScreenCube_prefab;
    [SerializeField] GameObject levelSelectCube_prefab;
    [SerializeField] GameObject bestTimesCube_prefab;
    [SerializeField] GameObject optionsCube_prefab;
    [SerializeField] GameObject creditsCube_prefab;
    [SerializeField] GameObject howToPlayCube_prefab;
    private float blockMass = 8.0f;
    private GameObject newCube_inst;
    [SerializeField] CanvasGroup canvasInputHandler;
    private bool startGameplayMusic;
    [SerializeField] AudioSource explodeSrc;
    // Start is called before the first frame update
    void Awake()
    {
        if (timeToDeleteExplosion < timeToHoldOnExplosion)
        {
            float tempDel = timeToDeleteExplosion;
            float tempHold = timeToHoldOnExplosion;

            timeToDeleteExplosion = tempHold;
            timeToHoldOnExplosion = tempDel;
        }
        timeToDeleteExplosion_orig = timeToDeleteExplosion;
        timeToHoldOnExplosion_orig = timeToHoldOnExplosion;

        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (FindDependencies())
        {
            canvasInputHandler.interactable = !explodeMode;
            canvasInputHandler.blocksRaycasts = !explodeMode;
            if (explodeMode == true)
            {
                Collider coll = gameObject.GetComponent<Collider>();
                if (coll != null)
                {
                    coll.enabled = false;
                }
                explodeScr.Explode(myRb, blockMass);
                timeToDeleteExplosion -= Time.fixedDeltaTime;
                timeToHoldOnExplosion -= Time.fixedDeltaTime;

                if (timeToHoldOnExplosion <= 0.0f)
                {
                    timeToHoldOnExplosion = 0.0f;
                    GameObject spawnObj = null;
                    startGameplayMusic = false;
                    switch (menuCube_enum)
                    {
                        case UpcomingMenuCube.TitleScreen:
                            spawnObj = titleScreenCube_prefab;
                            
                            break;

                        case UpcomingMenuCube.LevelSelect:
                            spawnObj = levelSelectCube_prefab;
                            startGameplayMusic = true;//set this bool based on whatever object the play button on the title screen spawns
                            break;
                        case UpcomingMenuCube.BestTimes:
                            spawnObj = bestTimesCube_prefab;
                            break;
                        case UpcomingMenuCube.Options:
                            spawnObj = optionsCube_prefab;
                            break;
                        case UpcomingMenuCube.Credits:
                            spawnObj = creditsCube_prefab;
                            break;
                        case UpcomingMenuCube.HowToPlay:
                            spawnObj = howToPlayCube_prefab;
                            startGameplayMusic = true;//also setting it here in case of first time play
                            break;

                    }
                    if (spawnObj != null)
                    {
                        if (newCube_inst == null)
                        {
                            newCube_inst = Instantiate(spawnObj, respawnPos, Quaternion.Euler(Vector3.zero), null);
                        }

                        if (startGameplayMusic == true)
                        {
                            
                            if (GameObject.FindGameObjectWithTag("musicPlayer") != null)
                            {
                                
                                if (GameObject.FindGameObjectWithTag("musicPlayer").GetComponent<MusicPlayer>() != null)
                                {
                                    MusicPlayer musicPlayer_scr = GameObject.FindGameObjectWithTag("musicPlayer").GetComponent<MusicPlayer>();
                                    musicPlayer_scr.PlayMusic(musicPlayer_scr.GetSongByName("gameplay"));
                                }
                            }
                        }
                       
                        Rigidbody newCubeRb = newCube_inst.GetComponent<Rigidbody>();
                        MenuCubeControl thisOnTheOtherCube = newCube_inst.GetComponent<MenuCubeControl>();

                        if (newCubeRb != null && thisOnTheOtherCube != null)
                        {
                            if (thisOnTheOtherCube.explodeMode == false)
                            {
                                newCubeRb.mass = blockMass;
                                newCubeRb.useGravity = false;
                                TweakedPhysics.RunTweakedGravity(newCubeRb);
                            }

                        }
                    }

                    if (timeToDeleteExplosion <= 0.0f)
                    {
                        timeToDeleteExplosion = 0.0f;
                        newCube_inst = null;
                        explodeMode = false;
                        Destroy(gameObject);
                    }
                }


            }
        }

    }

    bool FindDependencies()
    {
        bool ret = false;
        if (myRb == null)
        {
            if (gameObject.GetComponent<Rigidbody>() != null)
            {
                myRb = gameObject.GetComponent<Rigidbody>();
            }
        }

        if (explodeScr == null)
        {
            if (gameObject.GetComponent<ExplodeGeneric>() != null)
            {
                explodeScr = gameObject.GetComponent<ExplodeGeneric>();
            }
        }

        if (myRb != null && explodeScr!=null)
        {
            ret = true;
        }

        return ret;
    }

    public void GoToMenu(int nextMenu)
    {

        if (nextMenu < 0)
        {
            nextMenu = 0;
        }

        if(nextMenu > (int)Enum.GetValues(typeof(UpcomingMenuCube)).Cast<UpcomingMenuCube>().Last())
        {
            nextMenu = (int)Enum.GetValues(typeof(UpcomingMenuCube)).Cast<UpcomingMenuCube>().Last();
        }

        menuCube_enum = (UpcomingMenuCube)nextMenu;
        if (menuCube_enum == UpcomingMenuCube.BestTimes)
        {
            if (gameObject.GetComponent<LevelDisplay>() != null)
            {
                BestTimesCubeManager.levelRepresented = AllLevels.levelList[gameObject.GetComponent<LevelDisplay>().getCurrentLevelIndex()];
            }
            
        }
        explodeSrc.pitch = UnityEngine.Random.Range(0.8f, 1.2f);
        explodeSrc.Play();
        explodeMode = true;
    }


}
