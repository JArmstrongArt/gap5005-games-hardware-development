﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TweakedPhysics : MonoBehaviour
{
    public static void RunTweakedGravity(Rigidbody rb)
    {
        rb.AddForce(GetTweakedGravity(rb));
    }

    public static Vector3 GetTweakedGravity(Rigidbody rb)
    {
        return Physics.gravity * (rb.mass * rb.mass);//for some reason, unitys default physics divide the gravity by the mass, when to me i think the increased mass should only make you fall faster, so i multiply by mass to make it pure graity again, then multiply by the mass once more to make mass affect gravity in the inverse to the default of dividing.
    }
}
