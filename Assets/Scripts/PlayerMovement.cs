﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] Rigidbody playerRb;
    [SerializeField] float moveSpeed_rate;
    [SerializeField] float moveSpeed_max;
    [SerializeField] Vector2 moveSpeed_driftLimits;
    private float moveSpeed_rate_orig;
    public float moveSpeed_current;
    private Vector3 moveDirection;
    private Vector3 faceDirection;
    private Vector3 targetDestination;
    private bool targetDestination_set;
    private GameObject facingDestinationObj;
    private Camera mainCam;
    private bool tap_axisUsed;
    [SerializeField] PlayerInput inputScr;
    public bool forceStop;
    private Vector3 forceStop_prevRot;
    [SerializeField] GameObject moveSpot_prefab;
    private GameObject moveSpot_inst;
    [SerializeField] AudioSource skidSrc;
    [SerializeField] AudioSource engineSrc;
    // Start is called before the first frame update
    void Awake()
    {
        moveSpeed_rate_orig = moveSpeed_rate;
    }

    bool FindDependencies()
    {
        bool ret = false;

        if (mainCam == null)
        {
            if (GameObject.FindGameObjectWithTag("mainCam") != null)
            {
                if (GameObject.FindGameObjectWithTag("mainCam").GetComponent<Camera>() != null)
                {
                    mainCam = GameObject.FindGameObjectWithTag("mainCam").GetComponent<Camera>();
                }
            }
        }

        if (mainCam != null)
        {
            ret = true;
        }
        return ret;
    }

    private void OnDrawGizmos()
    {
        if (Constants.DEBUGMODE == true)
        {
            if (targetDestination_set == true)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawSphere(targetDestination, 0.3f);
            }
        }


    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (FindDependencies())
        {

            if (forceStop == false)
            {
                if (inputScr.getInputByName("Tap") > 0)
                {
                    if (tap_axisUsed == false)
                    {

                        Vector3 targetDestination_anyHeight = mainCam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Mathf.Abs(gameObject.transform.position.y - mainCam.gameObject.transform.position.y)));
                        //Debug.Log(targetDestination_anyHeight);
                        targetDestination = new Vector3(targetDestination_anyHeight.x, gameObject.transform.position.y, targetDestination_anyHeight.z);


                        targetDestination_set = true;
                        Vector3 targetDestination_moveSpotVersion = new Vector3(targetDestination.x, targetDestination.y - 0.35f, targetDestination.z);
                        if (moveSpot_inst == null)
                        {
                            moveSpot_inst =Instantiate(moveSpot_prefab, targetDestination_moveSpotVersion, moveSpot_prefab.transform.rotation, null);
                        }
                        else
                        {
                            moveSpot_inst.transform.position = targetDestination_moveSpotVersion;
                        }

                        tap_axisUsed = true;
                    }

                }
                else
                {
                    tap_axisUsed = false;
                }

                if (targetDestination_set == true)
                {





                    if (moveSpeed_current < moveSpeed_max)
                    {
                        moveSpeed_current += moveSpeed_rate * Time.fixedDeltaTime;
                    }

                    if (moveSpeed_current >= moveSpeed_max)
                    {
                        moveSpeed_current = moveSpeed_max;
                    }
                    if (facingDestinationObj == null)
                    {
                        facingDestinationObj = new GameObject();
                        facingDestinationObj.name = "facingDestinationObj";
                    }
                    if (facingDestinationObj != null)
                    {
                        facingDestinationObj.transform.position = gameObject.transform.position;
                        facingDestinationObj.transform.LookAt(new Vector3(targetDestination.x, gameObject.transform.position.y, targetDestination.z));

                        faceDirection = Vector3.Lerp(faceDirection, facingDestinationObj.transform.forward, Time.fixedDeltaTime * 10);
                        moveDirection = Vector3.Lerp(moveDirection, facingDestinationObj.transform.forward, Time.fixedDeltaTime * 5f);

                        if (Vector3.Distance(gameObject.transform.position, targetDestination) > 1.0f)
                        {
                            float engPitch = moveSpeed_current / moveSpeed_max;
                            if (engPitch < 0.4f)
                            {
                                engPitch = 0.4f;
                            }

                            engineSrc.pitch = engPitch;

                            float faceMoveDifference = 1 - Mathf.Abs(Vector3.Dot(faceDirection, moveDirection));
                            skidSrc.volume = faceMoveDifference * 0.8f;
                            if (faceMoveDifference < 0.8f)
                            {
                                faceMoveDifference = 0.8f;
                            }
                            skidSrc.pitch = faceMoveDifference + 0.4f;
                        }
                        else
                        {
                            engineSrc.pitch = Mathf.Lerp(engineSrc.pitch, 0.4f, 5 * Time.fixedDeltaTime);
                            skidSrc.volume = Mathf.Lerp(skidSrc.volume, 0, 2 * Time.fixedDeltaTime);
                            //skidSrc.pitch = Mathf.Lerp(skidSrc.pitch, 0.4f, 5 * Time.fixedDeltaTime);
                        }


                        playerRb.transform.forward = faceDirection;
                    }

                    if (Vector3.Dot(faceDirection, moveDirection) <= moveSpeed_driftLimits.y && Vector3.Dot(faceDirection, moveDirection) >= moveSpeed_driftLimits.x)
                    {
                        moveSpeed_rate = moveSpeed_rate_orig * (Vector3.Dot(faceDirection, moveDirection) + 1);
                    }
                    else if (Vector3.Dot(faceDirection, moveDirection) < moveSpeed_driftLimits.x)
                    {
                        moveSpeed_rate = moveSpeed_rate_orig * (moveSpeed_driftLimits.x + 1);
                    }
                    else
                    {
                        moveSpeed_rate = moveSpeed_rate_orig;
                    }
                    //Debug.Log(Vector3.Dot(faceDirection, moveDirection));//the more similar they are, the closer to 1 this value will be.
                }
                //moveDirection = moveDirection.normalized;
                //faceDirection = faceDirection.normalized;
                forceStop_prevRot = gameObject.transform.eulerAngles;
            }
            else
            {
                moveSpeed_current = Mathf.Lerp(moveSpeed_current, 0, 3.75f * Time.fixedDeltaTime);
                gameObject.transform.eulerAngles = forceStop_prevRot;
                skidSrc.volume = Mathf.Lerp(skidSrc.volume, 0, 2 * Time.fixedDeltaTime);
                skidSrc.pitch = Mathf.Lerp(skidSrc.pitch, 0, 5 * Time.fixedDeltaTime);
                engineSrc.volume = Mathf.Lerp(engineSrc.volume, 0, 2 * Time.fixedDeltaTime);
                engineSrc.pitch = Mathf.Lerp(engineSrc.pitch, 0.4f, 5 * Time.fixedDeltaTime);
            }




            playerRb.velocity = moveDirection * moveSpeed_current;
        }

    }

    public Vector3 MoveDirection_Get()
    {
        return moveDirection;
    }

    public float CurrentSpeed_Get()
    {
        return moveSpeed_current;
    }

    public float MaxSpeed_Get()
    {
        return moveSpeed_max;
    }
}
