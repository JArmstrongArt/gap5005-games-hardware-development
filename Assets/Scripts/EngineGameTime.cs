﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EngineGameTime : MonoBehaviour
{
    public static string EngineTimeToGameTime(float engineTime)
    {
        int milliseconds = Mathf.FloorToInt((engineTime - Mathf.Floor(engineTime)) * 100);
        int seconds = Mathf.FloorToInt(engineTime) % 60; 

        int minutes = Mathf.FloorToInt(engineTime / 60);

        if (minutes > 99)
        {
            minutes = 99;
        }

        if (seconds > 59)
        {
            seconds = 59;
        }

        if (milliseconds > 99)
        {
            milliseconds = 99;
        }

        return minutes.ToString() + ":" + seconds.ToString() + "." + milliseconds.ToString();
    }

    public static float GameTimeToEngineTime(string gameTime)
    {
        char[] chars = gameTime.ToCharArray();

        if (gameTime == Constants.INVALID_TIME)
        {
            return -1.0f;
        }

        bool reachedColon = false;
        bool reachedDot = false;

        string minutes = "";
        string seconds = "";
        string milliseconds = "";
        for (int i = 0; i < chars.Length; i++)
        {
            if (reachedColon == false && reachedDot == false)
            {
                if (chars[i] != ':')
                {
                    minutes += chars[i];
                    continue;
                }
                else
                {
                    reachedColon = true;
                    continue;
                }
            }

            if (reachedColon == true && reachedDot == false)
            {
                if (chars[i] != '.')
                {
                    seconds += chars[i];
                    continue;
                }
                else
                {
                    reachedDot = true;
                    continue;
                }
            }

            if (reachedColon == true && reachedDot == true)
            {
                milliseconds += chars[i];
                continue;
            }
        }

        return (float.Parse(minutes) * 60) + (float.Parse(seconds)) + (float.Parse(milliseconds) / 100);
    }

    public static string GameTime_Formalize(string gameTime)//the purpose of this function is to take game time and make it have a uniform layout to display in-game, do not use this in any form of data persistence because things will break!
    {
        string origGameTime = gameTime;
        gameTime = gameTime.Replace(":", " ");
        gameTime = gameTime.Replace(".", " ");
        string[] gameTime_split = gameTime.Split(' ');

        for(int i = 0; i < gameTime_split.Length; i++)
        {

            if (gameTime_split[i].Length > 2)
            {
                gameTime_split[i] = gameTime_split[i].Substring(gameTime_split[i].Length - 2, 2);
            }
            while(gameTime_split[i].Length < 2)
            {
                if (origGameTime == Constants.INVALID_TIME)
                {
                    gameTime_split[i] = "X" + gameTime_split[i];
                }
                else
                {
                    gameTime_split[i] = "0" + gameTime_split[i];
                }
                
            }
        }

        return gameTime_split[0] + ":" + gameTime_split[1] + "." + gameTime_split[2];
    }

}
