﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class AccelTest : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI accelTxt;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 tilt = Input.acceleration;
        if (accelTxt != null)
        {
            accelTxt.text = "accel: " + tilt.ToString();
        }
        tilt = Quaternion.Euler(new Vector3(90, 0, 0)) * tilt;
        gameObject.transform.forward = tilt.normalized;
    }
}
