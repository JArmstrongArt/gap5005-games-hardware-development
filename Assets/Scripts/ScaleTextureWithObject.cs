﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleTextureWithObject : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        ScaleIt();   
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void ScaleIt()
    {
        MeshRenderer rend = gameObject.GetComponent<MeshRenderer>();
        if (rend != null)
        {
            rend.material.SetTextureScale("_MainTex", new Vector2(1 / gameObject.transform.localScale.z, 1 / gameObject.transform.localScale.x));
        }
    }
}
