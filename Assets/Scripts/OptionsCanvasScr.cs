﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
public enum CarColours
{
    White,
    Black,
    Red,
    Blue,
    Green,
    Yellow,
    Purple,
    Orange,
    Pink,
    Brown
}
public class OptionsCanvasScr : MonoBehaviour
{
    private int curCol_int;
    private ColorByPrefs colPrefScr;
    private Spin spinScr;
    [SerializeField] AudioSource pingSrc;
    // Start is called before the first frame update
    void Awake()
    {
        curCol_int = (int)LoadCarColour();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    bool FindDependencies()
    {
        bool ret = false;
        if (colPrefScr == null)
        {
            if (GameObject.FindGameObjectWithTag("optionsCar") != null)
            {
                if (GameObject.FindGameObjectWithTag("optionsCar").GetComponent<ColorByPrefs>() != null)
                {
                    colPrefScr = GameObject.FindGameObjectWithTag("optionsCar").GetComponent<ColorByPrefs>();
                }
            }
        }

        if (spinScr == null)
        {
            if (GameObject.FindGameObjectWithTag("optionsCar") != null)
            {
                if (GameObject.FindGameObjectWithTag("optionsCar").GetComponent<Spin>() != null)
                {
                    spinScr = GameObject.FindGameObjectWithTag("optionsCar").GetComponent<Spin>();
                }
            }
        }

        if (colPrefScr != null && spinScr!=null)
        {
            ret = true;
        }

        return ret;
    }
    public void UpdateCarColour(int shiftAmount)
    {
        pingSrc.Play();
        curCol_int += shiftAmount;
        curCol_int = KeepCarColourInRange(curCol_int,true);

        PlayerPrefs.SetInt("racerCol", curCol_int);
        if (FindDependencies())
        {
            colPrefScr.UpdateColour();
            spinScr.spinFactor_current = spinScr.spinFactor * 8;

        }
    }

    public static int KeepCarColourInRange(int col, bool cyclic)
    {
        if (cyclic == false)
        {
            if (col < 0)
            {
                col = 0;
            }

            if (col > (int)Enum.GetValues(typeof(CarColours)).Cast<CarColours>().Last())
            {
                col = (int)Enum.GetValues(typeof(CarColours)).Cast<CarColours>().Last();
            }
        }
        else
        {


            if (col > (int)Enum.GetValues(typeof(CarColours)).Cast<CarColours>().Last())
            {
                col = Mathf.Abs((int)Enum.GetValues(typeof(CarColours)).Cast<CarColours>().Last() - Mathf.Abs( col))-1;
            }

            if (col < 0)
            {
                col = ((int)Enum.GetValues(typeof(CarColours)).Cast<CarColours>().Last() - Mathf.Abs(col)) + 1;
            }
        }

        return col;
    }

    public static CarColours LoadCarColour()
    {
        int colourInt = PlayerPrefs.GetInt("racerCol", 0);
        colourInt = KeepCarColourInRange(colourInt,false);
        return (CarColours)colourInt;
    }
}
