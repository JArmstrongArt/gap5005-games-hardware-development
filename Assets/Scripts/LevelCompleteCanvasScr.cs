﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;
public class LevelCompleteCanvasScr : MonoBehaviour
{
    private StarSet starSetObj;
    [SerializeField] GameObject starSetObj_parentObj;
    [SerializeField] Sprite star_filled;
    [SerializeField] Sprite star_unfilled;
    [SerializeField] TextMeshProUGUI timeTxt;
    [SerializeField] Image placementImg;
    [SerializeField] Sprite placement1Spr;
    [SerializeField] Sprite placement2Spr;
    [SerializeField] Sprite placement3Spr;
    private PlayerDamage dmgScr;
    private CountupTimer timeScr;
    private SaveLoad saveLoadScr;
    [SerializeField] GameObject nextLevelButtonObj;
    [SerializeField] RectTransform levelSelectButtonRect;
    [SerializeField] GameObject timerStuffObj;
    [SerializeField] CanvasGroup myCanvGroup;
    // Start is called before the first frame update
    void Awake()
    {
        starSetObj = new StarSet(starSetObj_parentObj, star_filled, star_unfilled);
        ImportantTasks();

        if (AllLevels.FindLevelIndexByName(SceneManager.GetActiveScene().name)>=AllLevels.levelList.Length-1)
        {
            nextLevelButtonObj.SetActive(false);
            timerStuffObj.SetActive(false);
            levelSelectButtonRect.anchoredPosition = new Vector2(0, levelSelectButtonRect.GetComponent<RectTransform>().anchoredPosition.y);
            levelSelectButtonRect.anchorMin = new Vector2(0.5f, 0);
            levelSelectButtonRect.anchorMax = new Vector2(0.5f, 0);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GoToLevelSelect()
    {
        MenuBlockInitSpawner.spawnCube = UpcomingMenuCube.LevelSelect;
        SceneManager.LoadScene("mainMenu");
    }

    public void CanvasVisible(bool state)
    {
        myCanvGroup.interactable = state;
        myCanvGroup.blocksRaycasts = state;
        if (state == true)
        {
            myCanvGroup.alpha = 1;
        }
        else
        {
            myCanvGroup.alpha = 0;
        }
    }

    public void GoToNextLevel()
    {
        int currentLevelIndex = AllLevels.FindLevelIndexByName(SceneManager.GetActiveScene().name);
        if (currentLevelIndex + 1 < AllLevels.levelList.Length)
        {
            SceneManager.LoadScene(AllLevels.levelList[currentLevelIndex + 1].sceneName);
        }
    }

    void DoRanking()
    {
        if (FindDependencies())
        {
            Level currentLevel = AllLevels.FindLevelByName(SceneManager.GetActiveScene().name);
            int finalRank = RankingCalculator.rankingCalculation(dmgScr, timeScr, currentLevel);
            starSetObj.SetRank(finalRank);
            LevelOverhead(finalRank, currentLevel);
        }
        
    }

    void LevelOverhead(int rank,Level lev)
    {
        if (FindDependencies())
        {
            if (rank > lev.starRanking)
            {
                lev.starRanking = rank;
            }

            AllLevels.AddTimeToLevel(timeScr.gameTime_get(),SceneManager.GetActiveScene().name);

            saveLoadScr.SaveGame();
        }

    }

    void RemoveHud()
    {
        if (GameObject.FindGameObjectWithTag("hudCanvas")!=null)
        {
            Destroy(GameObject.FindGameObjectWithTag("hudCanvas"));
        }
    }

    void PauseTimer()
    {
        if (FindDependencies())
        {
            timeScr.paused = true;
        }
    }

    void DoTime()
    {
        if (FindDependencies())
        {
            timeTxt.text = EngineGameTime.GameTime_Formalize( timeScr.gameTime_get());
            Level currentLevel = AllLevels.FindLevelByName(SceneManager.GetActiveScene().name);
            int timeInBestTimes_index = AllLevels.FindTimeIndexInLevel(currentLevel, timeScr.gameTime_get());

            if(timeInBestTimes_index>=0 && timeInBestTimes_index <= 2)
            {
                if (timeInBestTimes_index == 0)
                {
                    placementImg.sprite = placement1Spr;
                }
                else if (timeInBestTimes_index == 1)
                {
                    placementImg.sprite = placement2Spr;
                }
                else if (timeInBestTimes_index == 2)
                {
                    placementImg.sprite = placement3Spr;
                }
                placementImg.enabled = true;
            }
            else
            {
                placementImg.enabled = false;
            }


        }
    }
    void ImportantTasks()
    {
        PauseTimer();
        DoRanking();
        RemoveHud();
        DoTime();
    }

    bool FindDependencies()
    {
        bool ret = false;
        if (dmgScr == null)
        {
            if (GameObject.FindGameObjectWithTag("player") != null)
            {
                if (GameObject.FindGameObjectWithTag("player").GetComponent<PlayerDamage>() != null)
                {
                    dmgScr = GameObject.FindGameObjectWithTag("player").GetComponent<PlayerDamage>();
                }
            }
        }

        if (timeScr == null)
        {
            if (GameObject.FindGameObjectWithTag("hudCanvas") != null)
            {
                if (GameObject.FindGameObjectWithTag("hudCanvas").GetComponent<CountupTimer>() != null)
                {
                    timeScr = GameObject.FindGameObjectWithTag("hudCanvas").GetComponent<CountupTimer>();
                }
            }
        }

        if (saveLoadScr == null)
        {
            if (GameObject.FindGameObjectWithTag("allLevels") != null)
            {
                if (GameObject.FindGameObjectWithTag("allLevels").GetComponent<SaveLoad>()!=null)
                {
                    saveLoadScr = GameObject.FindGameObjectWithTag("allLevels").GetComponent<SaveLoad>();
                }
            }
        }



        if(dmgScr!=null && timeScr!=null && saveLoadScr!=null)
        {
            ret = true;
        }

        return ret;
    }
}
