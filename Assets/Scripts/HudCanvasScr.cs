﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class HudCanvasScr : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI damageTxt;
    [SerializeField] TextMeshProUGUI speedTxt;
    [SerializeField] TextMeshProUGUI timeTxt;
    [SerializeField] CountupTimer countupScr;
    private PlayerDamage damageScr;
    private PlayerMovement moveScr;
    
    private void Update()
    {
        if (FindDependencies())
        {
            timeTxt.text = "Time: " + EngineGameTime.GameTime_Formalize(countupScr.gameTime_get());

            damageTxt.text = "Health: " + Mathf.CeilToInt(100 - damageScr.dmg_taken).ToString();
            speedTxt.text = "Speed: " + Mathf.CeilToInt( moveScr.moveSpeed_current).ToString();

            damageTxt.color = Color.HSVToRGB(((100 - damageScr.dmg_taken) / 100)/3.6f, 1, 1);
        }

    }

    bool FindDependencies()
    {
        bool ret = false;

        if (damageScr == null)
        {
            if (GameObject.FindGameObjectWithTag("player") != null)
            {
                if (GameObject.FindGameObjectWithTag("player").GetComponent<PlayerDamage>() != null)
                {
                    damageScr = GameObject.FindGameObjectWithTag("player").GetComponent<PlayerDamage>();
                }
            }
        }

        if (moveScr == null)
        {
            if (GameObject.FindGameObjectWithTag("player") != null)
            {
                if (GameObject.FindGameObjectWithTag("player").GetComponent<PlayerMovement>() != null)
                {
                    moveScr = GameObject.FindGameObjectWithTag("player").GetComponent<PlayerMovement>();
                }
            }
        }

        if(moveScr!=null && damageScr != null)
        {
            ret = true;
        }

        return ret;
    }
}
