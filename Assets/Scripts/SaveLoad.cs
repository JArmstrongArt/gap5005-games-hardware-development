﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
enum EncryptMode
{
    Encrypt,
    Decrypt
}
//i implemented a cryptogram-style cypher to the save data to maintain the integrity of the file, AKA, prevent cheating.
public class SaveLoad : MonoBehaviour
{
    private char[,] cypherCombos = new char[69, 2];//26 letters lowercase + 26 letters uppercase + 10 numbers + 3 punctuation marks + space + this thing '|'+ a semicolon+the pound key= 69
    private AllLevels allLevScr;

    private bool dbgLoad_axisUsed;
    private bool dbgSave_axisUsed;


    // Start is called before the first frame update
    void Awake()
    {
        cypherCombos[0, 0] = 'a';
        cypherCombos[0, 1] = 'Q';
        cypherCombos[1, 0] = 'b';
        cypherCombos[1, 1] = 'Z';
        cypherCombos[2, 0] = 'c';
        cypherCombos[2, 1] = 'S';
        cypherCombos[3, 0] = 'd';
        cypherCombos[3, 1] = '0';
        cypherCombos[4, 0] = 'e';
        cypherCombos[4, 1] = 'D';
        cypherCombos[5, 0] = 'f';
        cypherCombos[5, 1] = '4';
        cypherCombos[6, 0] = 'g';
        cypherCombos[6, 1] = 'b';
        cypherCombos[7, 0] = 'h';
        cypherCombos[7, 1] = '.';
        cypherCombos[8, 0] = 'i';
        cypherCombos[8, 1] = 'U';
        cypherCombos[9, 0] = 'j';
        cypherCombos[9, 1] = 'G';
        cypherCombos[10, 0] = 'k';
        cypherCombos[10, 1] = '?';
        cypherCombos[11, 0] = 'l';
        cypherCombos[11, 1] = 'N';
        cypherCombos[12, 0] = 'm';
        cypherCombos[12, 1] = 'd';
        cypherCombos[13, 0] = 'n';
        cypherCombos[13, 1] = ' ';
        cypherCombos[14, 0] = 'o';
        cypherCombos[14, 1] = 'W';
        cypherCombos[15, 0] = 'p';
        cypherCombos[15, 1] = 'x';
        cypherCombos[16, 0] = 'q';
        cypherCombos[16, 1] = 'u';
        cypherCombos[17, 0] = 'r';
        cypherCombos[17, 1] = 'C';
        cypherCombos[18, 0] = 's';
        cypherCombos[18, 1] = '1';
        cypherCombos[19, 0] = 't';
        cypherCombos[19, 1] = 'I';
        cypherCombos[20, 0] = 'u';
        cypherCombos[20, 1] = 'B';
        cypherCombos[21, 0] = 'v';
        cypherCombos[21, 1] = 's';
        cypherCombos[22, 0] = 'w';
        cypherCombos[22, 1] = 't';
        cypherCombos[23, 0] = 'x';
        cypherCombos[23, 1] = 'k';
        cypherCombos[24, 0] = 'y';
        cypherCombos[24, 1] = 'T';
        cypherCombos[25, 0] = 'z';
        cypherCombos[25, 1] = 'f';
        cypherCombos[26, 0] = 'A';
        cypherCombos[26, 1] = 'K';
        cypherCombos[27, 0] = 'B';
        cypherCombos[27, 1] = 'v';
        cypherCombos[28, 0] = 'C';
        cypherCombos[28, 1] = 'h';
        cypherCombos[29, 0] = 'D';
        cypherCombos[29, 1] = '!';
        cypherCombos[30, 0] = 'E';
        cypherCombos[30, 1] = 'E';
        cypherCombos[31, 0] = 'F';
        cypherCombos[31, 1] = '5';
        cypherCombos[32, 0] = 'G';
        cypherCombos[32, 1] = 'y';
        cypherCombos[33, 0] = 'H';
        cypherCombos[33, 1] = 'A';
        cypherCombos[34, 0] = 'I';
        cypherCombos[34, 1] = 'g';
        cypherCombos[35, 0] = 'J';
        cypherCombos[35, 1] = 'w';
        cypherCombos[36, 0] = 'K';
        cypherCombos[36, 1] = 'z';
        cypherCombos[37, 0] = 'L';
        cypherCombos[37, 1] = 'n';
        cypherCombos[38, 0] = 'M';
        cypherCombos[38, 1] = 'j';
        cypherCombos[39, 0] = 'N';
        cypherCombos[39, 1] = '6';
        cypherCombos[40, 0] = 'O';
        cypherCombos[40, 1] = 'a';
        cypherCombos[41, 0] = 'P';
        cypherCombos[41, 1] = '#';
        cypherCombos[42, 0] = 'Q';
        cypherCombos[42, 1] = 'Y';
        cypherCombos[43, 0] = 'R';
        cypherCombos[43, 1] = 'o';
        cypherCombos[44, 0] = 'S';
        cypherCombos[44, 1] = 'c';
        cypherCombos[45, 0] = 'T';
        cypherCombos[45, 1] = 'M';
        cypherCombos[46, 0] = 'U';
        cypherCombos[46, 1] = 'q';
        cypherCombos[47, 0] = 'V';
        cypherCombos[47, 1] = 'i';
        cypherCombos[48, 0] = 'W';
        cypherCombos[48, 1] = 'V';
        cypherCombos[49, 0] = 'X';
        cypherCombos[49, 1] = '7';
        cypherCombos[50, 0] = 'Y';
        cypherCombos[50, 1] = '|';
        cypherCombos[51, 0] = 'Z';
        cypherCombos[51, 1] = 'r';
        cypherCombos[52, 0] = '0';
        cypherCombos[52, 1] = ':';
        cypherCombos[53, 0] = '1';
        cypherCombos[53, 1] = '2';
        cypherCombos[54, 0] = '2';
        cypherCombos[54, 1] = 'p';
        cypherCombos[55, 0] = '3';
        cypherCombos[55, 1] = 'R';
        cypherCombos[56, 0] = '4';
        cypherCombos[56, 1] = 'H';
        cypherCombos[57, 0] = '5';
        cypherCombos[57, 1] = '3';
        cypherCombos[58, 0] = '6';
        cypherCombos[58, 1] = 'X';
        cypherCombos[59, 0] = '7';
        cypherCombos[59, 1] = 'P';
        cypherCombos[60, 0] = '8';
        cypherCombos[60, 1] = 'm';
        cypherCombos[61, 0] = '9';
        cypherCombos[61, 1] = '9';
        cypherCombos[62, 0] = '.';
        cypherCombos[62, 1] = '8';
        cypherCombos[63, 0] = '?';
        cypherCombos[63, 1] = 'L';
        cypherCombos[64, 0] = '!';
        cypherCombos[64, 1] = 'J';
        cypherCombos[65, 0] = ' ';
        cypherCombos[65, 1] = 'O';
        cypherCombos[66, 0] = '|';
        cypherCombos[66, 1] = 'F';
        cypherCombos[67, 0] = ':';
        cypherCombos[67, 1] = 'e';
        cypherCombos[68, 0] = '#';
        cypherCombos[68, 1] = 'l';

        if (Constants.DEBUGMODE == true)
        {
            string testStr = "staccata madness! ";
            testStr = EncryptString(testStr);
            DecryptString(testStr);
            //EncryptString("level1|4|3|X:XX.XX|X:XX.XX|X:XX.XX|1|0#level2|2|3|X:XX.XX|X:XX.XX|X:XX.XX|0|0#level3|1|3|X:XX.XX|X:XX.XX|X:XX.XX|0|0");
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (Constants.DEBUGMODE == true)
        {
            if (Input.GetAxis("Debug_Save") > 0)
            {
                if (dbgSave_axisUsed == false)
                {
                    SaveGame();
                    dbgSave_axisUsed = true;
                }
            }
            else
            {
                dbgSave_axisUsed = false;
            }

            /*
            //I have decided to comment this out since I have only programmed loading for the scenario that the game just started, I'd have to program another method for mid-game loading which won't be a thing in the final product anyway.
            if (Input.GetAxis("Debug_Load") > 0)
            {
                if (dbgLoad_axisUsed == false)
                {
                    LoadGame();
                    dbgLoad_axisUsed = true;
                }
            }
            else
            {
                dbgLoad_axisUsed = false;
            }
            */
        }
    }

    string EncryptString(string input)
    {

        return EncDyc_main(input, EncryptMode.Encrypt);
    }

    public void SaveGame()
    {
        if (FindDependencies())//execute this so that allLevScr is set to something before the rest goes on
        {
            if (AllLevels.levelList_initialLoad_complete == true)//if the setup process of the various level lists is complete
            {
                string[,] levelStrings = new string[AllLevels.levelList.Length, 5+allLevScr.BestTimesLength_Get()];//make a new 2d array that will hold each saved variable for each level

                for (int i = 0; i < levelStrings.GetLength(0); i++)//for each level in levelStrings
                {
                    levelStrings[i, 0] = AllLevels.levelList[i].sceneName.ToString();//make the first value the sceneName of the current level
                    levelStrings[i, 1] = AllLevels.levelList[i].starRanking.ToString();//make the second value the starRanking of the current level
                    levelStrings[i, 2] = AllLevels.levelList[i].bestTimes.Length.ToString();//make the third value the number of best times in the current level, so that when this save is loaded it can be read properly past the list of best times

                    int bestTimesOffset = 0;//make a variable that acts as a fake index for which bestTime is currently being worked with in the upcoming for loop

                    for(int j = 0;j< AllLevels.levelList[i].bestTimes.Length; j++)//for the amount of best times that the save file will have
                    {
                        levelStrings[i, 3 + bestTimesOffset] = AllLevels.levelList[i].bestTimes[j].ToString();//starting after the latest variable in the level's save data, add each best time of the level
                        
                        bestTimesOffset += 1;//add 1 to bestTimesOffset so that it won't just record the first best time in the bestTimes array 3 times.
                    }
                    int bestTimesOffset_end = bestTimesOffset;//make a variable that copies the current value of bestTimesOffset, which also happens to be the index where you need to resume recording data after the best times have been recorded

                    int unlockInt = AllLevels.levelList[i].unlocked ? 1 : 0;//set an integer to represent the unlock bool, it will be 1 if the bool is true, and 0 if it is false
                    levelStrings[i, 3+bestTimesOffset_end] = unlockInt.ToString();//make the next value recorded for the level the state of the unlock bool for the level

                    int completedInt = AllLevels.levelList[i].completed ? 1 : 0;//set an integer to represent the completed bool, it will be 1 if the bool is true, and 0 if it is false
                    levelStrings[i, 3+bestTimesOffset_end+1] = completedInt.ToString();//make the next value recorded for the level the state of the completed bool for the level

                }

                string[] levelStrings_joined = new string[AllLevels.levelList.Length];//make a 1d array that represents a list of each level represented by individual strings with the level's variables connected by a common divider of '|'

                for(int i = 0; i < levelStrings_joined.Length; i++)//for each level to go into levelStrings_joined
                {
                    string[] stringParts = new string[levelStrings.GetLength(1)];//make an array that is destined to contain each of the levels variables in a 1d format temporarily
                    for(int j = 0; j < stringParts.Length; j++)//for each of the levels variables
                    {
                        stringParts[j] = levelStrings[i,j];//set the 1d array of stringParts appropriately element by element
                        
                    }
                    
                    levelStrings_joined[i] = string.Join("|", stringParts);//turn the current level into the earlier described connected form with those '|' seaprators
                }

                string finalSaveString = string.Join("#", levelStrings_joined);//turn the final data of several levels into each level divided by the common divider '#'

                finalSaveString = EncryptString(finalSaveString);//encrypt the save data using my cypher so nobody can cheat so easily, i will decrypt it when the data is loaded

                File.WriteAllText(Application.persistentDataPath + "/save.smiley", finalSaveString);//write the appropriate data to the save file
                
            }

        }
        
    }

    public void LoadGame_InitLoad()
    {
        if (FindDependencies())//execute this so that allLevScr is set to something before the rest goes on
        {
            if (File.Exists(Application.persistentDataPath + "/save.smiley"))//if the save file exists
            {

                string decodedLoad = DecryptString(File.ReadAllText(Application.persistentDataPath + "/save.smiley"));//run the save file through the decryptor, since the save data is stored after being encrypted by a cypher

                string[] loadedJoinedLevelStrings = decodedLoad.Split(new[] { "#" }, System.StringSplitOptions.None);//split the decoded save data into individual levels, since i made it so '#' is used to separate each level class object in the levellist.

                string[,] loadedLevelStrings = new string[loadedJoinedLevelStrings.Length, 5 + allLevScr.BestTimesLength_Get()];//make an empty 2d array representing every level's variables split into array elements.
                for (int i = 0; i < loadedLevelStrings.GetLength(0); i++)//for each level in loadedLevelStrings
                {
                    string[] currentSplit = loadedJoinedLevelStrings[i].Split(new[] { "|" }, System.StringSplitOptions.None);//make a new array comprised of the variables in the current level data at index 'i' in loadedLevelStrings.
                    for (int j = 0; j < loadedLevelStrings.GetLength(1); j++)//for each level variable in the current level data
                    {
                        loadedLevelStrings[i, j] = currentSplit[j];//set the current level and the current variable of the current level to the data at index 'j' in currentSplit.

                    }
                }

                for (int i = 0; i < loadedLevelStrings.GetLength(0); i++)//for each level in loadedLevelStrings
                {

                    allLevScr.LevelListInit_SceneName_Set(i,loadedLevelStrings[i, 0]);//set the sceneName in the initialLoad levelList to the first value of the current level data.
                    allLevScr.LevelListInit_StarRanking_Set(i, int.Parse( loadedLevelStrings[i, 1]));//set the starRanking in the initialLoad levelList to the second value of the current level data.

                    int bestTimesCount = int.Parse(loadedLevelStrings[i, 2]);//retrieve how many best times the save file was created with so that the data after the best times is accessed properly

                    for(int j = 0; j < bestTimesCount; j++)//for the amount of best times there are in the save file
                    {
                        allLevScr.LevelListInit_BestTime_Set(i, j, loadedLevelStrings[i, 3 + j]);//set each best time in the current level in the initialLoad levelList in the order they appear in the level data
                    }
                    int afterTimesIndex = 3 + bestTimesCount;//mark the first index after all the best times
                    if (int.Parse(loadedLevelStrings[i, afterTimesIndex]) == 0)//if the value representing the level unlock bool is equal to 0
                    {
                        allLevScr.LevelListInit_Unlocked_Set(i, false);//set the unlock state of the current level in the initialLoad levelList to false.
                    }
                    else
                    {
                        allLevScr.LevelListInit_Unlocked_Set(i, true);//set the unlock state of the current level in the initialLoad levelList to true.
                    }

                    if (int.Parse(loadedLevelStrings[i, afterTimesIndex+1]) == 0)//if the value representing the level completed bool is equal to 0
                    {
                        allLevScr.LevelListInit_Completed_Set(i, false);//set the completed state of the current level in the initialLoad levelList to false.
                    }
                    else
                    {
                        allLevScr.LevelListInit_Completed_Set(i, true);//set the completed state of the current level in the initialLoad levelList to true.
                    }
                }
            }
        }

    }

    string DecryptString(string input)
    {
        return EncDyc_main(input, EncryptMode.Decrypt);
    }

    string EncDyc_main(string input, EncryptMode eM)
    {
        int num0 = 0;
        int num1 = 1;

        if(eM == EncryptMode.Decrypt)
        {
            int numTemp = num0;
            num0 = num1;
            num1 = numTemp;
        }
        int currentIndex = 0;
        foreach (char c in input)
        {
            
            int resultingIndex = -1;
            for (int i = 0; i < cypherCombos.GetLength(0); i++)
            {
                if (cypherCombos[i, num0] == c)
                {
                    resultingIndex = i;
                    break;
                }
            }
            input = input.Remove(currentIndex, 1);
            if (resultingIndex >= 0)
            {
                input = input.Insert(currentIndex, cypherCombos[resultingIndex, num1].ToString());
            }
            currentIndex += 1;
        }
        if (Constants.DEBUGMODE == true)
        {
            Debug.Log(input);
        }

        return input;
    }

    bool FindDependencies()
    {
        bool ret = false;

        if (allLevScr == null)
        {
            if (gameObject.GetComponent<AllLevels>() != null)
            {
                allLevScr = gameObject.GetComponent<AllLevels>();
            }
        }

        if (allLevScr != null)
        {
            ret = true;
        }

        return ret;
    }
}
