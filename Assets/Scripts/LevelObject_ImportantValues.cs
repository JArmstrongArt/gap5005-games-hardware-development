﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;
public class StarSet
{
    private Image[] starIconImgs;

    private GameObject starParent;
    private Sprite star_filled;
    private Sprite star_unfilled;
    public StarSet(GameObject stP,Sprite starFilled,Sprite starUnfilled)
    {
        starParent = stP;
        star_filled = starFilled;
        star_unfilled = starUnfilled;
        CreateStarIconArray();
    }


    public void SetRank(int rank)
    {
        if (Length_Get() > 0)
        {
            if (rank < 0)
            {
                rank = 0;
            }

            if (rank > Length_Get())
            {
                rank = Length_Get();
            }

            for(int i = 0; i < Length_Get(); i++)
            {
                UnfillStar(i);
            }

            for(int i = 0;i< rank; i++)
            {
                FillStar(i);
            }
        }
    }
    void UnfillStar(int index)
    {
        starIconImgs[index].sprite = star_unfilled;
    }

    void FillStar(int index)
    {
        if (Length_Get() > 0)
        {
            if (index < 0)
            {
                index = 0;
            }

            if (index > Length_Get() - 1)
            {
                index = Length_Get() - 1;
            }

            starIconImgs[index].sprite = star_filled;
        }

    }

    void CreateStarIconArray()
    {

        int arrLen = 0;
        for (int i = 0; i < starParent.transform.childCount; i++)
        {
            if (starParent.transform.GetChild(i).gameObject.GetComponent<Image>() != null)
            {
                arrLen += 1;
            }
        }
        starIconImgs = new Image[arrLen];
        int starIndex = 0;
        for (int i = 0; i < starParent.transform.childCount; i++)
        {
            if (starParent.transform.GetChild(i).gameObject.GetComponent<Image>() != null)
            {
                starIconImgs[starIndex] = starParent.transform.GetChild(i).gameObject.GetComponent<Image>();
                starIndex += 1;
            }
        }
    }

    public int Length_Get()
    {
        return starIconImgs.Length;
    }

    public bool ArrayElement_IsNull(int index)
    {
        if (index < 0 || index >= Length_Get())
        {
            return true;
        }

        if (starIconImgs[index] != null)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    void ArrayElement_Visible(int index, bool visible)
    {
        starIconImgs[index].enabled = visible;
    }

    public void Visible(bool visible)
    {
        for (int i = 0; i < Length_Get(); i++)
        {
            ArrayElement_Visible(i, visible);
        }
    }
}
public class LevelObject_ImportantValues : MonoBehaviour
{
    public Image iconImg;

    public Image timerIcon;
    public TextMeshProUGUI timerTxt;
    public Image lockIcon;
    public StarSet starSetObj;
    [SerializeField] GameObject starSetObj_parentObj;
    [SerializeField] Sprite star_filled;
    [SerializeField] Sprite star_unfilled;
    public string levelToLoadName;
    public Button loadLevelBtn;
    // Start is called before the first frame update
    void Awake()
    {
        starSetObj = new StarSet(starSetObj_parentObj, star_filled, star_unfilled);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SimpleVisibility(bool showState)
    {
        iconImg.enabled = showState;
        timerIcon.enabled = showState;
        timerTxt.enabled = showState;
        lockIcon.enabled = showState;
        starSetObj.Visible(showState);
    }

    public void LoadMyLevel()
    {
        if (AllLevels.FindLevelByName(levelToLoadName) != null)
        {
            SceneManager.LoadScene(levelToLoadName);
        }
    }
}
