﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHalt : MonoBehaviour
{
    [SerializeField] PlayerFinish finishScr;
    [SerializeField] PlayerDamage damageScr;
    [SerializeField] PlayerInput inputScr;
    [SerializeField] PlayerMovement moveScr;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public void CheckHalt()
    {
        if (finishScr.levelFinished == true || damageScr.carDestroyed==true)
        {

            inputScr.input_enabled = false;
            moveScr.forceStop = true;
            damageScr.invincible = true;
        }
        else
        {
            inputScr.input_enabled = true;
            moveScr.forceStop = false;
            damageScr.invincible = false;
        }
    }
}
