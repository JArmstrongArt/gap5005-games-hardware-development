﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


[Serializable]
public enum rayDirections
{
    V3Up,
    V3Down,
    V3Left,
    V3Right,
    V3Forward,
    V3Backward,
    TransformUp,
    TransformDown,
    TransformLeft,
    TransformRight,
    TransformForward,
    TransformBackward,
    Custom


}

[Serializable]
public enum UpdateType
{
    Regular,
    Fixed,
    Late
}



public class GenericRay : MonoBehaviour
{

    public UpdateType updateType_enum;
    public Vector3 targetDir;
    public rayDirections direction;
    public float rayLength;
    private Ray ray;
    public RaycastHit ray_hit;

    public LayerMask ray_layers;
    public bool ray_active;
    public GameObject ray_surface;
    public Vector3 ray_hitPoint;
    public bool ray_disabled;

    public bool excludeFromDebugDraw;
    public Vector3 surfaceEulers;
    public Quaternion surfaceQuats;
    public Vector3 customDir_vec;
    public Color rayCol;
    // Use this for initialization
    void Awake()
    {

        ray_hitPoint = Vector3.zero;
    }

    void RayRoutine()
    {
        switch (direction)
        {
            case rayDirections.V3Up:
                targetDir = Vector3.up;
                break;
            case rayDirections.V3Down:
                targetDir = Vector3.down;
                break;
            case rayDirections.V3Left:
                targetDir = Vector3.left;
                break;
            case rayDirections.V3Right:
                targetDir = Vector3.right;
                break;
            case rayDirections.V3Forward:
                targetDir = Vector3.forward;
                break;
            case rayDirections.V3Backward:
                targetDir = Vector3.back;
                break;
            case rayDirections.TransformBackward:
                targetDir = -transform.forward;
                break;
            case rayDirections.TransformForward:
                targetDir = transform.forward;
                break;
            case rayDirections.TransformDown:
                targetDir = -transform.up;
                break;
            case rayDirections.TransformUp:
                targetDir = transform.up;
                break;
            case rayDirections.TransformRight:
                targetDir = transform.right;
                break;
            case rayDirections.TransformLeft:
                targetDir = -transform.right;
                break;
            case rayDirections.Custom:
                targetDir = customDir_vec;
                break;
        }

        ray = new Ray(gameObject.transform.position, targetDir);



        if (Physics.Raycast(ray, out ray_hit, rayLength, ray_layers) && ray_disabled == false)
        {

            ray_hitPoint = ray_hit.point;
            ray_surface = ray_hit.collider.gameObject;
            ray_active = true;



        }
        else
        {
            ray_active = false;
            ray_surface = null;
        }

        if (ray_surface == null)
        {
            surfaceEulers = new Vector3(-999, -999, -999);
            surfaceQuats = new Quaternion(-999, -999, -999, -999);
        }
        else
        {
            surfaceQuats = Quaternion.LookRotation(ray_hit.normal);
            surfaceEulers = surfaceQuats.eulerAngles;
        }



        if (excludeFromDebugDraw == false)
        {
            GroundedDraw();
        }
    }
    // Update is called once per frame
    void LateUpdate()
    {
        if(updateType_enum == UpdateType.Late)
        {
            RayRoutine();
        }


    }

    void FixedUpdate()
    {
        if (updateType_enum == UpdateType.Fixed)
        {
            RayRoutine();
        }


    }
    void Update()
    {
        if (updateType_enum == UpdateType.Regular)
        {
            RayRoutine();
        }


    }


    private void GroundedDraw()
    {

        Debug.DrawRay(gameObject.transform.position, targetDir * rayLength, rayCol);
    }


}
