﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum PathMovementMode
{
    LoopCut,
    Accelerometer_X,
    Accelerometer_Y
}
public class PathMovement : MonoBehaviour
{
    private Vector3[] pathPositions;
    [SerializeField] Transform pathPositionParent;
    [SerializeField] PathMovementMode pathMovementMode_enum;
    private int pathPositionIndex;
    [SerializeField] float moveSpeed;
    private float lerpPoint;

    // Start is called before the first frame update
    void Awake()
    {
        if (pathPositionParent != null)
        {
            pathPositionIndex = 0;
            pathPositions = new Vector3[pathPositionParent.childCount];
            for (int i = 0; i < pathPositionParent.childCount; i++)
            {
                pathPositions[i] = pathPositionParent.GetChild(i).transform.position;
            }

            gameObject.transform.position = pathPositions[pathPositionIndex];
        }
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 tilt = Input.acceleration;
        tilt = Quaternion.Euler(new Vector3(60, 0, 0)) * tilt;
        if (pathPositions.Length > 0)
        {
            switch (pathMovementMode_enum)
            {
                case (PathMovementMode.LoopCut):
                    if (lerpPoint < 1)
                    {
                        if (pathPositionIndex < pathPositions.Length - 1)
                        {
                            gameObject.transform.position = Vector3.Lerp(pathPositions[pathPositionIndex], pathPositions[pathPositionIndex + 1], lerpPoint);
                        }

                        lerpPoint += Time.deltaTime * moveSpeed;
                    }
                    else
                    {
                        pathPositionIndex += 1;
                        if (pathPositionIndex>= pathPositions.Length - 1)
                        {
                            pathPositionIndex = 0;
                            gameObject.transform.position = pathPositions[pathPositionIndex];
                        }

                        lerpPoint = 0;
                    }
                    break;
                case (PathMovementMode.Accelerometer_Y):
                    accelerometerMode(tilt.y * -1);
                    break;
                case (PathMovementMode.Accelerometer_X):
                    accelerometerMode(tilt.x * -1);
                    break;

            }

        }


    }

    void accelerometerMode(float input)
    {
        lerpPoint += Time.deltaTime * moveSpeed * input;

        if (lerpPoint <= 0)
        {
            lerpPoint = 0;
            if (pathPositionIndex > 0)
            {
                pathPositionIndex -= 1;
            }
            else
            {
                pathPositionIndex = 0;
            }
        }

        if (lerpPoint >= 1)
        {
            lerpPoint = 1;
            if (pathPositionIndex + 1 < pathPositions.Length - 1)
            {
                pathPositionIndex += 1;
            }
            else
            {
                pathPositionIndex = pathPositions.Length - 2;
            }
        }

        if (pathPositionIndex < pathPositions.Length - 1)
        {
            gameObject.transform.position = Vector3.Lerp(pathPositions[pathPositionIndex], pathPositions[pathPositionIndex + 1], lerpPoint);
        }

        
    }
}
