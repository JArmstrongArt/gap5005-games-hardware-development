﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDamage : MonoBehaviour
{
    private GameObject moveDirRay_inst;
    private GenericRay moveDirRay_inst_scr;
    private GenericRay moveDirRay_inst_scr_longReach;
    private PlayerMovement moveScr;
    [SerializeField] LayerMask rayLayers;
    [SerializeField] float dmg_max;
    private float dmg_calculated;
    private bool dmg_set;
    public float dmg_taken;
    public float dmg_taken_max;

    private GameObject moveDirRay_adjacent_inst;
    private GenericRay moveDirRay_adjacent_inst_scr;
    private GameObject moveDirRay_adjacent_worldRotationCopy_preAdjust_inst;
    private GameObject moveDirRay_adjacent_worldRotationCopy_postAdjust_inst;
    private GenericRay moveDirRay_adjacent_inst_scr_inverted;
    [SerializeField] float dmg_minor;
    private float dmg_calculated_minor;
    public bool invincible;
    public bool carDestroyed;
    [SerializeField] PlayerHalt haltScr;
    private CameraShake camShakeScr;
    [SerializeField] GameObject carModelRendererObj;
    [SerializeField] GameObject carExplode_prefab;
    [SerializeField] GameObject smokeParticleSystem_prefab;
    [SerializeField] GameObject levFailedCanvas_prefab;
    private GameObject levFailedCanvas_inst;
    [SerializeField] float timeAfterDeathForFailCanvas;
    [SerializeField] AudioSource hitSrc;
    [SerializeField] AudioSource[] explodeSrcList;
    private MusicPlayer musicPlayerScr;
    [SerializeField] AudioSource scrapeSrc;
    // Start is called before the first frame update
    void Awake()
    {
        Instantiate(smokeParticleSystem_prefab, gameObject.transform.position, smokeParticleSystem_prefab.transform.rotation, null);
    }

    // Update is called once per frame
    void Update()
    {


    }

    private void OnTriggerStay(Collider collision)
    {
        if (FindDependencies())
        {
            if (collision.gameObject.tag == "dangerZone")
            {
                if (carDestroyed == false)
                {
                    camShakeScr.shakeRange = 0.15f;
                    scrapeSrc.volume = Mathf.Lerp(scrapeSrc.volume, 0.7f, 4 * Time.deltaTime);
                }
                DamageTakenChange(dmg_minor * 6);
                
            }
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (FindDependencies())
        {
            if (other.gameObject.tag == "dangerZone")
            {

                scrapeSrc.volume = Mathf.Lerp(scrapeSrc.volume, 0, 2 * Time.deltaTime);
            }
        }
    }

    private void LateUpdate()
    {
        if (carDestroyed == true)
        {
            if (timeAfterDeathForFailCanvas > 0.0f)
            {
                timeAfterDeathForFailCanvas -= Time.deltaTime;
            }
            else
            {
                if (levFailedCanvas_inst == null)
                {
                    levFailedCanvas_inst = Instantiate(levFailedCanvas_prefab, Vector3.zero, Quaternion.Euler(Vector3.zero), null);
                }
            }

        }

        if (Input.GetAxis("Debug_Kill") > 0 && carDestroyed==false){
            Dbg_KillOnCommand();
        }

        if (FindDependencies())
        {
            if (carDestroyed == true)
            {
                scrapeSrc.volume = Mathf.Lerp(scrapeSrc.volume, 0, 2 * Time.deltaTime);
            }
            if (moveDirRay_inst == null)
            {
                moveDirRay_inst = new GameObject();
                moveDirRay_inst.name = "moveDirRay_inst";
            }

            if (moveDirRay_inst != null)
            {
                moveDirRay_inst.transform.position = gameObject.transform.position;
                if (moveDirRay_inst_scr == null)
                {
                    moveDirRay_inst_scr = moveDirRay_inst.AddComponent<GenericRay>();
                    moveDirRay_inst_scr.direction = rayDirections.Custom;
                    moveDirRay_inst_scr.customDir_vec = moveScr.MoveDirection_Get().normalized;
                    moveDirRay_inst_scr.rayLength = 0.95f;
                    moveDirRay_inst_scr.rayCol = Color.blue;
                    moveDirRay_inst_scr.ray_layers = rayLayers;
                }

                if (moveDirRay_inst_scr != null)
                {
                    moveDirRay_inst_scr.customDir_vec = moveScr.MoveDirection_Get().normalized;

                    if (moveDirRay_inst_scr_longReach == null)
                    {
                        moveDirRay_inst_scr_longReach = moveDirRay_inst.AddComponent<GenericRay>();
                        moveDirRay_inst_scr_longReach.direction = moveDirRay_inst_scr.direction;
                        moveDirRay_inst_scr_longReach.customDir_vec = moveDirRay_inst_scr.customDir_vec;
                        moveDirRay_inst_scr_longReach.rayLength = 1.35f;
                        moveDirRay_inst_scr_longReach.rayCol = Color.green;
                        moveDirRay_inst_scr_longReach.ray_layers = moveDirRay_inst_scr.ray_layers;
                    }

                    if (moveDirRay_inst_scr_longReach != null)
                    {
                        moveDirRay_inst_scr_longReach.customDir_vec = moveDirRay_inst_scr.customDir_vec;

                        if (moveDirRay_inst_scr.ray_active == true)
                        {
                            if (dmg_set == false)
                            {
                                dmg_calculated = dmg_max * (moveScr.CurrentSpeed_Get() / moveScr.MaxSpeed_Get()) * Vector3.Dot(moveScr.MoveDirection_Get(), -moveDirRay_inst_scr.ray_hit.normal);
                                if (carDestroyed == false)
                                {
                                    camShakeScr.shakeRange = dmg_calculated/5.5f;
                                }
                                hitSrc.pitch = Random.Range(0.8f, 1.2f);
                                hitSrc.Play();
                                DamageTakenChange(dmg_calculated);
                                //PrintDamage();
                                dmg_set = true;
                            }
                            
                        }

                        if (moveDirRay_inst_scr_longReach.ray_active == false)
                        {
                            dmg_set = false;
                        }

                        if (moveDirRay_adjacent_inst == null)
                        {
                            moveDirRay_adjacent_inst = new GameObject();
                            moveDirRay_adjacent_inst.name = "moveDirRay_adjacent_inst";
                        }

                        if (moveDirRay_adjacent_inst != null)
                        {
                            moveDirRay_adjacent_inst.transform.position = moveDirRay_inst.transform.position;

                            if (moveDirRay_adjacent_worldRotationCopy_preAdjust_inst == null)
                            {
                                moveDirRay_adjacent_worldRotationCopy_preAdjust_inst = new GameObject();
                                moveDirRay_adjacent_worldRotationCopy_preAdjust_inst.name = "moveDirRay_adjacent_worldRotationCopy_preAdjust_inst";
                            }

                            if (moveDirRay_adjacent_worldRotationCopy_preAdjust_inst != null)
                            {
                                if (moveScr.MoveDirection_Get().normalized != Vector3.zero)//only here to remove that annoying 'look rotation viewing vector is zero' print call.
                                {
                                    moveDirRay_adjacent_worldRotationCopy_preAdjust_inst.transform.rotation = Quaternion.LookRotation(moveScr.MoveDirection_Get().normalized);
                                }
                                

                                if (moveDirRay_adjacent_worldRotationCopy_postAdjust_inst == null) {
                                    moveDirRay_adjacent_worldRotationCopy_postAdjust_inst = new GameObject();
                                    moveDirRay_adjacent_worldRotationCopy_postAdjust_inst.name = "moveDirRay_adjacent_worldRotationCopy_postAdjust_inst";
                                }

                                if (moveDirRay_adjacent_worldRotationCopy_postAdjust_inst != null)
                                {
                                    Vector3 preEuler = moveDirRay_adjacent_worldRotationCopy_preAdjust_inst.transform.eulerAngles;
                                    moveDirRay_adjacent_worldRotationCopy_postAdjust_inst.transform.rotation = Quaternion.Euler(preEuler.x, preEuler.y + 90.0f, preEuler.z);

                                    if (moveDirRay_adjacent_inst_scr == null)
                                    {
                                        moveDirRay_adjacent_inst_scr = moveDirRay_adjacent_inst.AddComponent<GenericRay>();
                                        moveDirRay_adjacent_inst_scr.direction = rayDirections.Custom;
                                        moveDirRay_adjacent_inst_scr.customDir_vec = moveDirRay_adjacent_worldRotationCopy_postAdjust_inst.transform.forward;
                                        moveDirRay_adjacent_inst_scr.rayLength = 0.7f;
                                        moveDirRay_adjacent_inst_scr.rayCol = Color.red;
                                        moveDirRay_adjacent_inst_scr.ray_layers = rayLayers;
                                    }

                                    if (moveDirRay_adjacent_inst_scr != null)
                                    {
                                        moveDirRay_adjacent_inst_scr.customDir_vec = moveDirRay_adjacent_worldRotationCopy_postAdjust_inst.transform.forward;

                                        if (moveDirRay_adjacent_inst_scr_inverted == null)
                                        {
                                            moveDirRay_adjacent_inst_scr_inverted = moveDirRay_adjacent_inst.AddComponent<GenericRay>();
                                            moveDirRay_adjacent_inst_scr_inverted.direction = moveDirRay_adjacent_inst_scr.direction;
                                            moveDirRay_adjacent_inst_scr_inverted.customDir_vec = -moveDirRay_adjacent_inst_scr.customDir_vec;
                                            moveDirRay_adjacent_inst_scr_inverted.rayLength = moveDirRay_adjacent_inst_scr.rayLength;
                                            moveDirRay_adjacent_inst_scr_inverted.rayCol = Color.yellow;
                                            moveDirRay_adjacent_inst_scr_inverted.ray_layers = moveDirRay_adjacent_inst_scr.ray_layers;
                                        }

                                        if (moveDirRay_adjacent_inst_scr_inverted != null)
                                        {
                                            moveDirRay_adjacent_inst_scr_inverted.customDir_vec = -moveDirRay_adjacent_inst_scr.customDir_vec;

                                            if(moveDirRay_adjacent_inst_scr.ray_active==true || moveDirRay_adjacent_inst_scr_inverted.ray_active == true)
                                            {
                                                if (carDestroyed == false)
                                                {
                                                    camShakeScr.shakeRange = 0.15f;
                                                    scrapeSrc.volume = Mathf.Lerp(scrapeSrc.volume, 0.7f, 4 * Time.deltaTime);
                                                }
                                                
                                                dmg_calculated_minor = dmg_minor * (moveScr.CurrentSpeed_Get() / moveScr.MaxSpeed_Get());
                                                DamageTakenChange(dmg_calculated_minor);
                                                
                                            }
                                            else
                                            {
                                                scrapeSrc.volume = Mathf.Lerp(scrapeSrc.volume, 0.0f, 2 * Time.deltaTime);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

    }

    bool FindDependencies()
    {
        bool ret = false;

        if (musicPlayerScr == null)
        {
            if (GameObject.FindGameObjectWithTag("musicPlayer") != null)
            {
                if (GameObject.FindGameObjectWithTag("musicPlayer").GetComponent<MusicPlayer>() != null)
                {
                    musicPlayerScr = GameObject.FindGameObjectWithTag("musicPlayer").GetComponent<MusicPlayer>();
                }
            }
        }

        if (moveScr == null)
        {
            moveScr = gameObject.GetComponent<PlayerMovement>();
        }

        if (camShakeScr == null)
        {
            if (GameObject.FindGameObjectWithTag("mainCam")!=null)
            {
                if (GameObject.FindGameObjectWithTag("mainCam").GetComponent<CameraShake>() != null)
                {
                    camShakeScr = GameObject.FindGameObjectWithTag("mainCam").GetComponent<CameraShake>();
                }
            }
        }

        if (moveScr != null && camShakeScr!=null && musicPlayerScr!=null)
        {
            ret = true;
        }

        return ret;
    }

    void DamageTakenChange(float changeVal)
    {
        if(invincible == false)
        {
            if (dmg_taken + changeVal > dmg_taken_max)
            {
                dmg_taken = dmg_taken_max;
            }
            else if (dmg_taken + changeVal < 0)
            {
                dmg_taken = 0;
            }
            else
            {
                dmg_taken += changeVal;
            }

            if (dmg_taken >= dmg_taken_max)
            {
                if (carDestroyed == false)
                {
                    GameObject carExplode_inst = Instantiate(carExplode_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                    if (carExplode_inst.GetComponent<PlayerExplode>() != null)
                    {
                        carExplode_inst.GetComponent<PlayerExplode>().modelSrc = carModelRendererObj;
                        if (carModelRendererObj.GetComponent<MeshRenderer>() != null)
                        {
                            carModelRendererObj.GetComponent<MeshRenderer>().enabled = false;
                        }
                        
                    }
                    PlayExplodeSound();
                    if (FindDependencies())
                    {
                        musicPlayerScr.StopMusic();
                    }
                    carDestroyed = true;
                }
                

                haltScr.CheckHalt();
            }
        }


    }

    void Dbg_KillOnCommand()
    {
        dmg_taken = dmg_taken_max;
        DamageTakenChange(0);
    }

    void PrintDamage()
    {
        Debug.Log("Damage Taken: " + dmg_calculated.ToString() + "|Total Damage: " + dmg_taken.ToString() + "||MoveDir Dot: " + Vector3.Dot(moveScr.MoveDirection_Get(), -moveDirRay_inst_scr.ray_hit.normal).ToString());
    }

    void PlayExplodeSound()
    {
        int explodeIndex = Random.Range(0, explodeSrcList.Length);
        AudioSource explodeSrc = explodeSrcList[explodeIndex];
        explodeSrc.pitch = Random.Range(0.8f, 1.2f);
        explodeSrc.Play();
    }
}
