﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class BestTimesCubeManager : MonoBehaviour
{
    [SerializeField] Transform allTimesParent;
    public static Level levelRepresented = null;
    [SerializeField] Image levelIconImg;
    // Start is called before the first frame update
    void Awake()
    {
        if (levelRepresented != null)
        {
            levelIconImg.sprite = levelRepresented.icon;
            int timeWriteIndex = 0;
            if (allTimesParent.childCount > 0)
            {
                for (int i = 0; i < allTimesParent.childCount; i++)
                {
                    if (allTimesParent.GetChild(i).GetComponent<TextMeshProUGUI>() != null)
                    {
                        TextMeshProUGUI textComp = allTimesParent.GetChild(i).GetComponent<TextMeshProUGUI>();
                        if (timeWriteIndex< levelRepresented.bestTimes.Length)
                        {
                            textComp.text = EngineGameTime.GameTime_Formalize(levelRepresented.bestTimes[timeWriteIndex]);
                        }
                        else
                        {
                            textComp.text = EngineGameTime.GameTime_Formalize( Constants.INVALID_TIME);
                        }
                        timeWriteIndex += 1;
                }
                }
            }
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
