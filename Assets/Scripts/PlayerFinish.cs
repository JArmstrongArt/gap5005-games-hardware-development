﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerFinish : MonoBehaviour
{
    public bool levelFinished;

    [SerializeField] PlayerHalt haltScr;
    [SerializeField] GameObject levCompleteCanvas_prefab;

    [SerializeField] float timeBeforeLevCompleteCanvas;
    private bool levCompleteTimer;
    private bool canvasSpawned;
    [SerializeField] AudioSource hornSrc;
    private LevelCompleteCanvasScr levCompCanvasScr;
    // Start is called before the first frame update


    void Awake()
    {
        levCompleteTimer = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (levCompCanvasScr != null)
        {
            if (levCompleteTimer == true)
            {
                timeBeforeLevCompleteCanvas -= Time.deltaTime;
                if (timeBeforeLevCompleteCanvas <= 0)
                {
                    if (canvasSpawned == false)
                    {
                        if (levCompCanvasScr != null)
                        {
                            levCompCanvasScr.CanvasVisible(true);
                        }
                        canvasSpawned = true;
                    }
                }
            }
        }

    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "finishArea")
        {
            if (levelFinished == false)
            {

                AllLevels.levelList[AllLevels.FindLevelIndexByName(SceneManager.GetActiveScene().name)].completed = true;
                if (AllLevels.FindLevelIndexByName(SceneManager.GetActiveScene().name) + 1 < AllLevels.levelList.Length)
                {
                    AllLevels.levelList[AllLevels.FindLevelIndexByName(SceneManager.GetActiveScene().name) + 1].unlocked = true;
                }
                GameObject levCompCanvObj= Instantiate(levCompleteCanvas_prefab, Vector3.zero, Quaternion.Euler(Vector3.zero), null);
                levCompCanvasScr = levCompCanvObj.GetComponent<LevelCompleteCanvasScr>();
                if (levCompCanvasScr != null)
                {
                    levCompCanvasScr.CanvasVisible(false);
                }
                hornSrc.Play();
                levCompleteTimer = true;

                levelFinished = true;
            }
            haltScr.CheckHalt();
        }


    }


}
