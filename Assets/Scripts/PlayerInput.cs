﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class inputValue
{
    public string input_name;
    //public bool enabled;
    [HideInInspector]
    public float input_value;
    [HideInInspector]
    public float input_value_lerped;
    public float input_value_lerped_strength;
}
public class PlayerInput : MonoBehaviour
{
    public bool input_enabled;
    [SerializeField]
    private inputValue[] allInputValues;
    // Start is called before the first frame update
    void Awake()
    {
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < allInputValues.Length; i++)
        {
            if (input_enabled == true)
            {
                allInputValues[i].input_value = Input.GetAxis(allInputValues[i].input_name.ToString());
                allInputValues[i].input_value_lerped = Mathf.Lerp(allInputValues[i].input_value_lerped, allInputValues[i].input_value, allInputValues[i].input_value_lerped_strength * Time.deltaTime);
            }
            else
            {
                allInputValues[i].input_value = 0;
                allInputValues[i].input_value_lerped = 0;

            }
            
        }
    }

    public float getInputByName(string inputName, bool lerped)
    {
        float ret = 0;
        for(int i = 0; i < allInputValues.Length; i++)
        {
            if (allInputValues[i].input_name == inputName)
            {
                if (lerped == false)
                {
                    ret = allInputValues[i].input_value;
                }
                else
                {
                    ret = allInputValues[i].input_value_lerped;
                }
                
            }
        }
        return ret;
    }





    public float getInputByName(string inputName)
    {
        float ret = 0;
        for (int i = 0; i < allInputValues.Length; i++)
        {
            if (allInputValues[i].input_name == inputName)
            {
                ret = allInputValues[i].input_value;

            }
        }
        return ret;
    }
}
