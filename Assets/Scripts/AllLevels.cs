﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

[Serializable]
public class Level
{


    public string sceneName;
    public int starRanking;
    public string[] bestTimes;
    public Sprite icon;
    public bool unlocked;
    public bool completed;
    public float avgTimeToBeat_engineTime;

}
public class AllLevels : MonoBehaviour
{
    public static Level[] levelList;
    [SerializeField] Level[] levelList_initialLoad;
    public static bool levelList_initialLoad_complete;
    [SerializeField] SaveLoad saveLoadScr;
    private int bestTimesLength = 3;
    [SerializeField]
    private Level[] dbg_levelList_viewableInInspector;

    public int BestTimesLength_Get()
    {
        return bestTimesLength;
    }
    public int LevelListInit_Length()
    {
        return levelList_initialLoad.Length;
    }



    public string LevelListInit_SceneName_Get(int index)
    {
        return levelList_initialLoad[index].sceneName;
    }

    public int LevelListInit_StarRanking_Get(int index)
    {
        return levelList_initialLoad[index].starRanking;
    }

    public string LevelListInit_BestTime_Get(int index, int index_time)
    {
        return levelList_initialLoad[index].bestTimes[index_time];
    }

    public Sprite LevelListInit_Icon_Get(int index)
    {
        return levelList_initialLoad[index].icon;
    }

    public bool LevelListInit_Unlocked_Get(int index)
    {
        return levelList_initialLoad[index].unlocked;
    }

    public bool LevelListInit_Completed_Get(int index)
    {
        return levelList_initialLoad[index].completed;
    }

    public float LevelListInit_AvgTimeToBeat_EngineTime_Get(int index)
    {
        return levelList_initialLoad[index].avgTimeToBeat_engineTime;
    }



    public void LevelListInit_SceneName_Set(int index, string sN)
    {
        levelList_initialLoad[index].sceneName = sN;
    }

    public void LevelListInit_StarRanking_Set(int index, int sR)
    {
        levelList_initialLoad[index].starRanking = sR;
    }

    public void LevelListInit_BestTime_Set(int index, int index_time, string timeVal)
    {
        levelList_initialLoad[index].bestTimes[index_time] = timeVal;
    }

    public void LevelListInit_Icon_Set(int index, Sprite icn)
    {
        levelList_initialLoad[index].icon = icn;
    }

    public void LevelListInit_Unlocked_Set(int index, bool state)
    {
        levelList_initialLoad[index].unlocked = state;
    }

    public void LevelListInit_Completed_Set(int index, bool state)
    {
        levelList_initialLoad[index].completed = state;
    }

    public void LevelListInit_AvgTimeToBeat_EngineTime_Set(int index, float val)
    {
        levelList_initialLoad[index].avgTimeToBeat_engineTime = val;
    }

    private void Update()
    {
        if (Constants.DEBUGMODE == true)
        {
            dbg_levelList_viewableInInspector = levelList;
        }
    }
    private void Awake()
    {
        InitLevelList();
    }

    void InitLevelList()
    {
        if (levelList_initialLoad_complete == false)//if this is the very first time the script is Awake, since this variable is static
        {
            //do this for loop before the save is loaded
            for (int i = 0; i < levelList_initialLoad.Length; i++)//for each level in the default settings to be written into levelList
            {
                levelList_initialLoad[i].starRanking = 0;//set the default of starRanking to 0
                levelList_initialLoad[i].bestTimes = new string[BestTimesLength_Get()];//set the default of the bestTimes array to an empty array of a definite size.
                for (int j = 0; j < levelList_initialLoad[i].bestTimes.Length; j++)//for each element in the bestTimes array
                {
                    levelList_initialLoad[i].bestTimes[j] = Constants.INVALID_TIME;//set the time to be the null representation 'X:XX.XX'.
                }
                levelList_initialLoad[i].completed = false;//set the completed bool of the level to false.
            }
            saveLoadScr.LoadGame_InitLoad();//load the save file into initialLoad, we will use the rest of this 'levelList_initialLoad_complete' nest to write InitialLoad to the real levelList.
            levelList = new Level[levelList_initialLoad.Length];//define levelList as a completely empty array of null objects that is the same length as the default list it will be based on.


            for (int i = 0; i < levelList.Length; i++)//for every level that will be in levelList
            {
                levelList[i] = new Level();//make sure the current level is not completely null anymore so errors are not produced.
                Level_DeepCopy(levelList_initialLoad[i], levelList[i]);//manually copy each element of the corresponding initialLoad level element so that the values remain individual from one another in memory.
            }

            levelList_initialLoad_complete = true;//set this to true, preventing this nest from running ever again unless i set this variable to false for no good reason and a new scene begins.
        }
    }

    void Level_DeepCopy(Level level_from, Level level_to)
    {

        level_to.sceneName = level_from.sceneName;
        level_to.starRanking = level_from.starRanking;

        level_to.bestTimes = new string[level_from.bestTimes.Length];

        for(int i = 0; i < level_to.bestTimes.Length; i++)
        {
            level_to.bestTimes[i] = level_from.bestTimes[i];
        }

        level_to.icon = level_from.icon;
        level_to.unlocked = level_from.unlocked;
        level_to.completed = level_from.completed;
        level_to.avgTimeToBeat_engineTime = level_from.avgTimeToBeat_engineTime;
    }

    static public Level FindLevelByName(string levName)
    {
        Level ret = null;

        for(int i = 0; i < levelList.Length; i++)
        {
            if (levelList[i].sceneName == levName)
            {
                ret = levelList[i];
                break;
            }
        }
        return ret;
    }

    static public int FindLevelIndexByName(string levName)
    {
        int ret = -1;

        for (int i = 0; i < levelList.Length; i++)
        {
            if (levelList[i].sceneName == levName)
            {
                ret = i;
                break;
            }
        }
        return ret;
    }

    static public void AddTimeToLevel(string gameTime,string levName)
    {
        float engineTime = EngineGameTime.GameTimeToEngineTime(gameTime);

        Level curLevel = AllLevels.FindLevelByName(levName);
        if (engineTime >= 0)//GameTimeToEngineTime returns -1 if the game time provided to it is invalid.
        {
            for(int i = 0; i < curLevel.bestTimes.Length; i++)//for each element
            {
                if(engineTime<EngineGameTime.GameTimeToEngineTime(curLevel.bestTimes[i])|| curLevel.bestTimes[i] == Constants.INVALID_TIME)//if enginetime is quicker than the current time in the loop or the current time in the loop is the first invalid encountered
                {
                    int pushIndex = curLevel.bestTimes.Length - 1;//push index is end of array
                    while (pushIndex > i)//while pushindex is more than or equal to the time we're quicker than
                    {
                        if (pushIndex > 0)//if pushindex is not the first element
                        {
                            curLevel.bestTimes[pushIndex] = curLevel.bestTimes[pushIndex - 1];//element at pushindex is now the element before pushindex
                        }
                        
                        pushIndex -=1;//-1 from pushindex
                    }

                    curLevel.bestTimes[i] = gameTime;//set i to gametime
                    break;//very important, end loop early

                }
            }
        }
    }

    static public int FindTimeIndexInLevel(Level lev,string gameTime)
    {
        int ret = -1;
        if (lev.bestTimes.Length > 0)
        {
            for (int i = 0; i < lev.bestTimes.Length; i++)
            {
                if (lev.bestTimes[i] == gameTime)
                {
                    ret = i;
                    break;
                }
            }
        }

        return ret;
    }
}