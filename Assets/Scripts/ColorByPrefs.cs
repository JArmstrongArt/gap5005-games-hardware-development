﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class ColorByPrefs : MonoBehaviour
{

    // Start is called before the first frame update
    void Awake()
    {
        UpdateColour();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateColour()
    {

        CarColours colourEnum = OptionsCanvasScr.LoadCarColour();
        Renderer objRend = gameObject.GetComponent<Renderer>();
        if (objRend != null)
        {
            if (objRend.materials.Length > 0)
            {
                for (int i = 0; i < objRend.materials.Length; i++)
                {
                    if (objRend.materials[i] != null)
                    {
                        if (objRend.materials[i].name.ToLower().Contains("_changablecolour"))
                        {
                            Material curMat = objRend.materials[i];
                            switch (colourEnum)
                            {
                                case CarColours.Black:
                                    curMat.SetColor("_Color", new Color(0.2f, 0.2f, 0.2f, 1.0f));
                                    break;
                                case CarColours.Blue:
                                    curMat.SetColor("_Color", Color.blue);
                                    break;
                                case CarColours.Brown:
                                    curMat.SetColor("_Color", new Color(0.6156862745098039f, 0.4705882352941176f, 0.3843137254901961f));
                                    break;
                                case CarColours.Green:
                                    curMat.SetColor("_Color", Color.green);
                                    break;
                                case CarColours.Orange:
                                    curMat.SetColor("_Color", new Color(0.9803921568627451f, 0.6901960784313725f, 0.0196078431372549f));
                                    break;
                                case CarColours.Pink:
                                    curMat.SetColor("_Color", new Color(0.9490196078431373f, 0.5333333333333333f, 0.8313725490196078f));
                                    break;
                                case CarColours.Purple:
                                    curMat.SetColor("_Color", new Color(0.596078431372549f, 0.1843137254901961f, 0.8156862745098039f));
                                    break;
                                case CarColours.Red:
                                    curMat.SetColor("_Color", Color.red);
                                    break;
                                case CarColours.White:
                                    curMat.SetColor("_Color", Color.white);
                                    break;
                                case CarColours.Yellow:
                                    curMat.SetColor("_Color", Color.yellow);
                                    break;
                                default:
                                    curMat.SetColor("_Color", Color.white);
                                    break;
                            }
                        }
                        


                    }
                }

            }
        }
    }

}
