﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerExplode : MonoBehaviour
{
    [HideInInspector]
    public GameObject modelSrc;
    private MeshFilter modelDisplay;
    private MeshRenderer modelRenderer;
    private Rigidbody myRb;
    private ExplodeGeneric explodeScr;

    // Start is called before the first frame update
    void Awake()
    {

    }


    // Update is called once per frame
    void FixedUpdate()
    {
        if (FindDependencies())
        {
            if (modelSrc != null)
            {
                gameObject.transform.localScale = modelSrc.transform.localScale;
                if (modelSrc.GetComponent<MeshFilter>() != null)
                {
                    
                    if (modelSrc.GetComponent<MeshFilter>().mesh != null)
                    {
                        if(modelDisplay.mesh!= modelSrc.GetComponent<MeshFilter>().mesh)
                        {
                            modelDisplay.mesh = modelSrc.GetComponent<MeshFilter>().mesh;
                        }
                        
                        if (modelSrc.GetComponent<MeshRenderer>() != null)
                        {


                            modelRenderer.materials = modelSrc.GetComponent<MeshRenderer>().materials;


                            if (modelDisplay.mesh != null && modelRenderer !=null)
                            {

                                explodeScr.Explode(myRb, 8.0f);

                            }
                        }
                    }




                }

            }
        }
    }

    bool FindDependencies()
    {
        bool ret = false;
        if (modelRenderer == null)
        {
            if (gameObject.GetComponent<MeshRenderer>() != null)
            {
                modelRenderer = gameObject.GetComponent<MeshRenderer>();
            }
        }

        if (modelDisplay == null)
        {
            if (gameObject.GetComponent<MeshFilter>() != null)
            {
                modelDisplay = gameObject.GetComponent<MeshFilter>();
            }
        }

        if (myRb == null)
        {
            if (gameObject.GetComponent<Rigidbody>() != null)
            {
                myRb = gameObject.GetComponent<Rigidbody>();
            }
        }

        if (explodeScr == null)
        {
            if (gameObject.GetComponent<ExplodeGeneric>() != null)
            {
                explodeScr = gameObject.GetComponent<ExplodeGeneric>();
            }
        }


        if (modelRenderer != null && modelDisplay != null && myRb!=null && explodeScr!=null)
        {
            ret = true;
        }
        return ret;
    }
}
