﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarSmokeManager : MonoBehaviour
{
    private PlayerDamage dmgScr;
    private GameObject playerObj;
    private GameObject playerExplodeObj;
    private ParticleSystem pS;

    //a lot of this stuff could be stored as vector2 variables and be better for me, but i feel if a designer were to try and use that it would be less intuitive
    [SerializeField] Vector2 smoke_developmentPercentBoundaries;
    private Vector2 smoke_softHardPercentBoundaries_converted;
    [SerializeField] Color smokeColour_soft;
    [SerializeField] Color smokeColour_hard;
    [SerializeField] Color smokeColour_dead;
    [SerializeField] float smokeOpacity_soft;
    [SerializeField] float smokeOpacity_hard;
    [SerializeField] float smokeOpacity_dead;
    [SerializeField] float smokeRiseSpeed_alive;
    [SerializeField] float smokeRiseSpeed_dead;
    // Start is called before the first frame update
    void Awake()
    {

        DevPercBoundClamper();
        SwapCheck();
        //this is its own private variable so that when it converts, it doesnt confuse designers looking at the inspector
        smoke_softHardPercentBoundaries_converted = new Vector2(smoke_developmentPercentBoundaries.x / 100, smoke_developmentPercentBoundaries.y / 100);
    }

    void SwapCheck()
    {
        //in my specific instance, i need the 2 values to be in ascending order for a comparison in update
        if (smoke_developmentPercentBoundaries.y < smoke_developmentPercentBoundaries.x)
        {
            float tempX = smoke_developmentPercentBoundaries.x;
            float tempY = smoke_developmentPercentBoundaries.y;
            smoke_developmentPercentBoundaries = new Vector2(tempY, tempX);
        }

        if (smokeOpacity_soft > smokeOpacity_hard)
        {
            float tempSoft = smokeOpacity_soft;
            float tempHard = smokeOpacity_hard;
            smokeOpacity_soft = tempHard;
            smokeOpacity_hard = tempSoft;
        }
    }
    void DevPercBoundClamper()
    {
        //also cannot have a percent below 0
        if (smoke_developmentPercentBoundaries.x <0)
        {
            smoke_developmentPercentBoundaries = new Vector2(0, Mathf.Abs(smoke_developmentPercentBoundaries.y));
        }

        if (smoke_developmentPercentBoundaries.y <0)
        {
            smoke_developmentPercentBoundaries = new Vector2(Mathf.Abs(smoke_developmentPercentBoundaries.x), 0);
        }

        //also cannot have a percent over 100
        if (smoke_developmentPercentBoundaries.x > 100)
        {
            smoke_developmentPercentBoundaries = new Vector2(100, Mathf.Abs(smoke_developmentPercentBoundaries.y));
        }

        if (smoke_developmentPercentBoundaries.y > 100)
        {
            smoke_developmentPercentBoundaries = new Vector2(Mathf.Abs(smoke_developmentPercentBoundaries.x), 100);
        }


    }
    // Update is called once per frame
    void Update()
    {
        if (FindDependencies())
        {
            float percentDmg = (1 / dmgScr.dmg_taken_max) * dmgScr.dmg_taken;
            Color smokeColour_current = Color.white;
            
            ParticleSystem.MainModule ma = pS.main;
            if (dmgScr.carDestroyed == false)
            {
                ma.startSpeed = smokeRiseSpeed_alive;
                if (percentDmg < smoke_softHardPercentBoundaries_converted.x)
                {
                    smokeColour_current = new Color(smokeColour_soft.r, smokeColour_soft.g, smokeColour_soft.b, 0);
                }
                if(percentDmg>= smoke_softHardPercentBoundaries_converted.x && percentDmg <= smoke_softHardPercentBoundaries_converted.y)
                {
                    float boundaryDifference = Mathf.Abs(smoke_softHardPercentBoundaries_converted.x - smoke_softHardPercentBoundaries_converted.y);
                    float percentDmg_boundaryDiffRelative = Mathf.Abs(percentDmg - smoke_softHardPercentBoundaries_converted.x);
                    float percentFinal = (1 / boundaryDifference) * percentDmg_boundaryDiffRelative;

                    Color startCol_noAlpha = Color.Lerp(smokeColour_soft, smokeColour_hard, percentFinal);

                    float opacityDifference = Mathf.Abs(smokeOpacity_hard - smokeOpacity_soft);
                    float opacitySet = smokeOpacity_soft + (opacityDifference * percentFinal);
                    smokeColour_current = new Color(startCol_noAlpha.r, startCol_noAlpha.g, startCol_noAlpha.b, opacitySet);
                }

                if (percentDmg > smoke_softHardPercentBoundaries_converted.y)
                {
                    smokeColour_current = new Color(smokeColour_hard.r, smokeColour_hard.g, smokeColour_hard.b, smokeOpacity_hard);
                }
            }
            else
            {
                ma.startSpeed = smokeRiseSpeed_dead;
                smokeColour_current = new Color(smokeColour_dead.r, smokeColour_dead.g, smokeColour_dead.b, smokeOpacity_dead);
            }
            ma.startColor = smokeColour_current;



        }
    }

    private void LateUpdate()
    {
        if (FindDependencies())
        {
            Vector3 playerObjPos_raised = new Vector3(playerObj.transform.position.x, playerObj.transform.position.y + 0.5f, playerObj.transform.position.z);
            if (dmgScr.carDestroyed == false)
            {
                gameObject.transform.position = playerObjPos_raised;
            }
            else
            {
                if (playerExplodeObj != null)
                {
                    gameObject.transform.position = playerExplodeObj.transform.position;
                }
                else
                {
                    gameObject.transform.position = playerObjPos_raised;
                }
            }
        }

    }

    bool FindDependencies()
    {
        bool ret = false;
        if (dmgScr == null)
        {
            if (GameObject.FindGameObjectWithTag("player") != null)
            {
                if (GameObject.FindGameObjectWithTag("player").GetComponent<PlayerDamage>() != null)
                {
                    dmgScr = GameObject.FindGameObjectWithTag("player").GetComponent<PlayerDamage>();
                }
            }
        }

        if (playerObj == null)
        {
            if (GameObject.FindGameObjectWithTag("player") != null)
            {
                playerObj = GameObject.FindGameObjectWithTag("player");
            }
        }

        if (playerExplodeObj == null)
        {
            if (GameObject.FindGameObjectWithTag("player_explode") != null)
            {
                playerObj = GameObject.FindGameObjectWithTag("player_explode");
            }
        }

        if (pS == null)
        {
            if (gameObject.GetComponent<ParticleSystem>() != null)
            {
                pS = gameObject.GetComponent<ParticleSystem>();
            }
        }

        if(dmgScr!=null && playerObj != null && pS!=null)//INTENTIONALLY leaving out playerExplodeObj, only exists once player is dead, will do checks for playerExplodeObj being null in other loops.
        {
            ret = true;
        }
        return ret;
    }
}
