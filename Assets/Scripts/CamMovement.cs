﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamMovement : MonoBehaviour
{
    private GameObject playerObj;
    private PlayerFinish finishScr;
    private Vector3 origPos;
    private CameraShake camShakeScr;
    private Vector3 lerpPos;
    // Start is called before the first frame update
    void Awake()
    {
        origPos = gameObject.transform.position;
        lerpPos = origPos;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (FindDependencies())
        {
            if (finishScr.levelFinished == true)
            {
                lerpPos = Vector3.Lerp(lerpPos, new Vector3(playerObj.transform.position.x, playerObj.transform.position.y+25, playerObj.transform.position.z), 3 * Time.deltaTime);
            }
            else
            {
                lerpPos = Vector3.Lerp(lerpPos, origPos, 4 * Time.deltaTime);
            }

            gameObject.transform.position = lerpPos + camShakeScr.shakeVector;
        }
    }

    bool FindDependencies()
    {
        bool ret = false;
        if (playerObj == null)
        {
            if (GameObject.FindGameObjectWithTag("player") != null)
            {
                playerObj = GameObject.FindGameObjectWithTag("player");
            }
        }

        if (finishScr == null)
        {
            if (GameObject.FindGameObjectWithTag("player") != null)
            {
                if (GameObject.FindGameObjectWithTag("player").GetComponent<PlayerFinish>() != null)
                {
                    finishScr = GameObject.FindGameObjectWithTag("player").GetComponent<PlayerFinish>();
                }
            }
        }

        if (camShakeScr == null)
        {
            if (gameObject.GetComponent<CameraShake>() != null)
            {
                camShakeScr = gameObject.GetComponent<CameraShake>();
            }
        }

        if (playerObj != null && finishScr != null && camShakeScr!=null)
        {
            ret = true;
        }
        return ret;
    }
}
