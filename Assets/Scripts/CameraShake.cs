﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    public float shakeRange;
    [SerializeField] float shakeRate;
    private Vector3 prevPos;
    [HideInInspector]
    public Vector3 shakeVector;
    // Start is called before the first frame update
    void Awake()
    {
        shakeVector = Vector3.zero;
    }

    // Update is called once per frame
    void Update()
    {
        if (shakeRange > 0)
        {

            shakeVector = new Vector3(Random.Range(-shakeRange, shakeRange), 0, Random.Range(-shakeRange, shakeRange));
            shakeRange -= Time.deltaTime*shakeRate;
        }
        else
        {
            shakeVector = Vector3.zero;
            shakeRange = 0;
        }
    }
}
