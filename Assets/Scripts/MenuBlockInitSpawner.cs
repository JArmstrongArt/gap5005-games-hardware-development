﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuBlockInitSpawner : MonoBehaviour
{
    private Vector3 spawnPoint = Vector3.zero;
    public static UpcomingMenuCube spawnCube = UpcomingMenuCube.TitleScreen;
    [SerializeField] GameObject titleScreenCube_prefab;
    [SerializeField] GameObject levelSelectCube_prefab;
    [SerializeField] GameObject bestTimesCube_prefab;
    [SerializeField] GameObject optionsCube_prefab;
    [SerializeField] GameObject creditsCube_prefab;
    [SerializeField] GameObject howToPlayCube_prefab;
    // Start is called before the first frame update
    void Awake()
    {
        switch (spawnCube)
        {
            case UpcomingMenuCube.TitleScreen:
                Instantiate(titleScreenCube_prefab, spawnPoint, Quaternion.Euler(Vector3.zero), null);
                break;
            case UpcomingMenuCube.LevelSelect:
                Instantiate(levelSelectCube_prefab, spawnPoint, Quaternion.Euler(Vector3.zero), null);
                break;
            case UpcomingMenuCube.BestTimes:
                Instantiate(bestTimesCube_prefab, spawnPoint, Quaternion.Euler(Vector3.zero), null);
                break;
            case UpcomingMenuCube.Options:
                Instantiate(optionsCube_prefab, spawnPoint, Quaternion.Euler(Vector3.zero), null);
                break;
            case UpcomingMenuCube.Credits:
                Instantiate(creditsCube_prefab, spawnPoint, Quaternion.Euler(Vector3.zero), null);
                break;
            case UpcomingMenuCube.HowToPlay:
                Instantiate(howToPlayCube_prefab, spawnPoint, Quaternion.Euler(Vector3.zero), null);
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
