﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PlayerDebugCommands : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Constants.DEBUGMODE == true)
        {
            if (Input.GetAxis("Debug_LevelSelect") > 0)
            {
                SceneManager.LoadScene("mainMenu");
            }

            if (Input.GetAxis("Debug_Restart") > 0)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }
    }
}
